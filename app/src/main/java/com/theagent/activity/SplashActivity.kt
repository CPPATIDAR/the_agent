package com.theagent.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import com.theagent.R
import com.theagent.constant.Constants
import com.theagent.util.LocaleManager
import com.theagent.util.SharedPref

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        Handler().postDelayed(Runnable {
            Log.e("TAG",SharedPref.getSharedPreferences(applicationContext,SharedPref.NAME).toString())
            val data:String = SharedPref.getSharedPreferences(applicationContext,SharedPref.NAME)
            if(!SharedPref.getSharedPreferences(applicationContext,SharedPref.NAME).isNullOrEmpty()){
                Constants.setHeader(SharedPref.getSharedPreferences(applicationContext,SharedPref.AUTH_TYPE),SharedPref.getSharedPreferences(applicationContext,SharedPref.TOKEN_DATA))
                if(SharedPref.getSharedPreferences(applicationContext,SharedPref.USER_TYPE) == "player"){
                    startActivity(Intent(this,HomeActivity::class.java))
                    finish()
                }else{
                    startActivity(Intent(this,AgentActivity::class.java))
                    finish()
                }

            }else{
                startActivity(Intent(this,LandingActivity::class.java))
                finish()
            }

        },3000)

    }
}
