package com.theagent.activity

import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.internal.NavigationMenuView
import com.google.android.material.navigation.NavigationView
import com.theagent.R
import com.theagent.fragment.RoundedBottomSheetDialogFragment
import com.theagent.inherited_classes.BaseActivity
import com.theagent.util.SharedPref
import kotlinx.android.synthetic.main.activity_agent.*

class AgentActivity : BaseActivity(), RoundedBottomSheetDialogFragment.BottomSheetListnerr {

    internal lateinit var listener: AgentActivity.BottomClick

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_agent)
        initView()
        disableMenuScroll(nav_view_agent)
    }

    private fun initView(){
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_agent_fragment) as NavHostFragment?
        NavigationUI.setupWithNavController(bottom_navigation_agent, navHostFragment!!.navController)
        NavigationUI.setupWithNavController(nav_view_agent, navHostFragment!!.navController)

        val headerView = nav_view_agent.getHeaderView(0)
        initheaderLayout(headerView)
    }

    fun initheaderLayout(headerView: View){
        val img_nav_menu = headerView.findViewById<ImageView>(R.id.img_nav_menu)
        val ll_subs = headerView.findViewById<LinearLayout>(R.id.ll_subs)

        val ll_logout = headerView.findViewById<LinearLayout>(R.id.ll_logout)


        ll_logout.setOnClickListener { v: View? ->
            showDialog()
        }



        ll_subs.setOnClickListener { v: View? ->
            drawer_layout.closeDrawer(Gravity.LEFT)
            val navController: NavController = Navigation.findNavController(this !!,R.id.nav_host_fragment)
//            if(navController.getCurrentDestination()!!.getId() == R.id.homeFragment){
//                navController.navigate(R.id.action_homeFragment_to_transactionHistoryFragment)
//            }else if(navController.getCurrentDestination()!!.getId() == R.id.profileFragment){
//                navController.navigate(R.id.action_profileFragment_to_userStatsFragment)
//
//            }else if(navController.getCurrentDestination()!!.getId() == R.id.inboxFragment){
//
//            }

        }


        img_nav_menu.setOnClickListener { v: View? ->
            drawer_layout.closeDrawer(Gravity.LEFT)
        }
    }

    fun showDialog(){
        val md_dialog = MaterialAlertDialogBuilder(this)
        md_dialog.setTitle(R.string.logout_text)
            .setPositiveButton(R.string.ok, DialogInterface.OnClickListener{ dialog, which ->
                SharedPref.clearSharedPreferences(applicationContext)
                startActivity(Intent(this,LandingActivity::class.java))
                finish()
            })
            .setNegativeButton(R.string.cancel, DialogInterface.OnClickListener{ dialog, which ->
                dialog.dismiss()
            })
        md_dialog.show()
    }

    override fun cameraClick(isVideo: Boolean) {
        listener.onCameraLick(isVideo)
    }

    override fun galleryClick(isVideo: Boolean) {
        listener.onGalleryClick(isVideo)
    }

    fun initBottomInterface(switchInterface:BottomClick){
        listener = switchInterface
    }


    interface BottomClick {
        fun onCameraLick(isVideo:Boolean)
        fun onGalleryClick(isVideo:Boolean)
    }

    private fun disableMenuScroll(navView: NavigationView) {
        val navMenu = navView.getChildAt(0) as NavigationMenuView
        navMenu.layoutManager = object : LinearLayoutManager(this) {
            override fun canScrollVertically(): Boolean {
                return false
            }
        }
    }

}
