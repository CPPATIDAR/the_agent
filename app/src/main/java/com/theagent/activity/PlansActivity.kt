package com.theagent.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.viewpager.widget.ViewPager
import com.google.gson.JsonObject
import com.theagent.R
import com.theagent.adapter.Pager
import com.theagent.api.AgentServiceGenerator
import com.theagent.api.ServiceInterface
import com.theagent.inherited_classes.BaseActivity
import com.theagent.models.BaseModel
import com.theagent.util.SharedPref
import kotlinx.android.synthetic.main.activity_plans.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PlansActivity : BaseActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_plans)
        super.init()

        val type = intent.getIntExtra("type",-1)
        getPlans(type)
    }


    fun getPlans(type:Int){
        showIndicator("Fetching Plans")
        val obj = JsonObject()
        obj.addProperty("type",type)
        obj.addProperty("lan_type",SharedPref.getSharedPreferences(applicationContext,SharedPref.LANGUAGE))
        val apiCall = AgentServiceGenerator.create().create(ServiceInterface::class.java)
        apiCall.getPlans(obj).enqueue(object:Callback<BaseModel>{
            override fun onFailure(call: Call<BaseModel>, t: Throwable) {
                Log.e("TAG",t.localizedMessage)
                hideIndicator()
            }

            override fun onResponse(call: Call<BaseModel>, response: Response<BaseModel>) {
                hideIndicator()
                val mainResp = response.body()!!
                Log.e("TAG",mainResp.data!!.size.toString())
                if(mainResp.status!!){
                    setUppageAdapter(mainResp)
                }
            }

        })
    }

    fun setUppageAdapter(mainResp:BaseModel){
        tabs.setupWithViewPager(pager)
        pager.adapter = Pager(this@PlansActivity,mainResp.data)
        if(mainResp.data!!.isNotEmpty()){
            for (pos in 0 until mainResp.data.size){
                tabs.getTabAt(pos)!!.setText(mainResp.data.get(pos).name)
            }

        }
        pager.addOnPageChangeListener(object:ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
               tabs.getTabAt(position)!!.setText(mainResp.data!!.get(position).name)
            }

        })

    }
}
