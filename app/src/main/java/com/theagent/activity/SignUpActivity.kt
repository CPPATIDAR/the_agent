package com.theagent.activity

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import com.google.android.material.snackbar.Snackbar
import com.google.gson.JsonObject
import com.theagent.R
import com.theagent.adapter.CustomAdapter
import com.theagent.api.AgentServiceGenerator
import com.theagent.api.ServiceInterface
import com.theagent.inherited_classes.BaseActivity
import com.theagent.models.BaseModel
import com.theagent.models.Data
import com.theagent.models.LoginBase
import com.theagent.util.SharedPref
import kotlinx.android.synthetic.main.activity_sign_up.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SignUpActivity : BaseActivity() {

    var spinner_list = listOf<Data>()
    var country_list = listOf<Data>()
    var state_list = listOf<Data>()
    var city_list = listOf<Data>()
    val EMAIL_REGEX = "^[A-Za-z](.*)([@]{1})(.{1,})(\\.)(.{1,})";
    private var country_id:String ? = null
    private var plan_id:String? = null
    private var terms_bool:Boolean = false
    private var state_id:String ? = null
    private var city_id:String ? = null
    private var _id:String ? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        super.init()

        plan_id = intent.getStringExtra("plan_id")
        Log.e("TAG",plan_id)
        getRegion()
        initListner()
    }

    fun initListner(){

        nation_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View?,
                position: Int,
                id: Long
            ) {
                if(spinner_list.size>0){

                    country_id =  spinner_list.get(position).id.toString()
                    getCountry()
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }

        country_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View?,
                position: Int,
                id: Long
            ) {
                if(country_list.size>0){

                    state_id =  country_list.get(position).id.toString()
                    getState()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }

        state_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View?,
                position: Int,
                id: Long
            ) {
                if(state_list.size>0){

                    city_id =  state_list.get(position).id.toString()
                    getCity()
                }
            }
            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }

        city_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View?,
                position: Int,
                id: Long
            ) {
                if(city_list.size>0){

                    _id =  city_list.get(position).id.toString()
                    getCity()
                }
            }
            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }

        checkbox_terms.setOnCheckedChangeListener { buttonView, isChecked ->
            terms_bool = isChecked
        }


        btn_register.setOnClickListener { v: View? ->

            if(!et_name.text.isNullOrEmpty()){
                ti_name.error = ""
                if(isEmailValid(et_email.text.toString())){
                    ti_mail.error = ""
                    if(!et_phone.text.isNullOrEmpty()){
                        ti_phone.error= ""
                        if(!et_address.text.isNullOrEmpty()){
                            ti_address.error = ""
                                if(!et_pswd.text.isNullOrEmpty()){
                                    ti_pswd.error = ""
                                    if(!et_confpswd.text.isNullOrEmpty()){
                                        ti_conf_pswd.error = ""
                                       if(et_pswd.text.toString().equals(et_confpswd.text.toString())){
                                           if(!et_matches.text.isNullOrEmpty()){
                                               ti_match.error = ""
                                               if(terms_bool){
                                                   registeruser()
                                               }else{
                                                   Snackbar.make(constraint_layout, R.string.terms, Snackbar.LENGTH_SHORT)
                                                       .show()
                                               }
                                           }else{
                                               ti_match.error = "Please enter matches"
                                           }
                                       }else{
                                           Snackbar.make(constraint_layout, R.string.pswd_match, Snackbar.LENGTH_SHORT)
                                               .show()
                                       }
                                    }else{
                                        ti_conf_pswd.error = "Please enter confirm password"
                                    }
                                }else{
                                    ti_pswd.error = "Please enter password"
                                }

                        }else{
                            ti_address.error = "Please enter address"
                        }
                    }else{
                        ti_phone.error = "Please enter number"
                    }
                }else{
                    ti_mail.error = "Please enter valid email"
                }
            }else{
                ti_name.error = "Please enter name"
            }

        }

        tv_already_login.setOnClickListener { v: View? ->
            startActivity(Intent(this,LoginActivity::class.java))
        }

    }

    fun getRegion(){
        showIndicator("Fetching Country")
        val obj = JsonObject()
        obj.addProperty("language",SharedPref.getSharedPreferences(applicationContext,SharedPref.LANGUAGE))
        val apiCall = AgentServiceGenerator.create().create(ServiceInterface::class.java)
        apiCall.getRegion(obj).enqueue(object :Callback<BaseModel>{
            override fun onFailure(call: Call<BaseModel>, t: Throwable) {
              Log.e("TAG",t.printStackTrace().toString())
                hideIndicator()
            }

            override fun onResponse(call: Call<BaseModel>, response: Response<BaseModel>) {
               Log.e("TAG",response.message())
                hideIndicator()
                if(response.isSuccessful){
                    val mainResp = response.body()!!
                    if(mainResp.status!!){
                        spinner_list = mainResp.data!!
                        val customAdapter = CustomAdapter(applicationContext, mainResp.data!!)
                        nation_spinner.adapter = customAdapter
                    }
                }
            }

        })
    }

    fun getCountry(){

        val obj = JsonObject()
        obj.addProperty("language",SharedPref.getSharedPreferences(applicationContext,SharedPref.LANGUAGE))
        obj.addProperty("region_id",country_id)
        val apiCall = AgentServiceGenerator.create().create(ServiceInterface::class.java)
        apiCall.getCountry(obj).enqueue(object :Callback<BaseModel>{
            override fun onFailure(call: Call<BaseModel>, t: Throwable) {
                Log.e("TAG",t.printStackTrace().toString())

            }

            override fun onResponse(call: Call<BaseModel>, response: Response<BaseModel>) {
                Log.e("TAG",response.message())

                if(response.isSuccessful){
                    val mainResp = response.body()!!
                    if(mainResp.status!!){
                        country_list = mainResp.data!!
                        val customAdapter = CustomAdapter(applicationContext, mainResp.data!!)
                        country_spinner.adapter = customAdapter
                    }
                }
            }

        })
    }

    fun getState(){

        val obj = JsonObject()
        obj.addProperty("language",SharedPref.getSharedPreferences(applicationContext,SharedPref.LANGUAGE))
        obj.addProperty("country_id",state_id)
        val apiCall = AgentServiceGenerator.create().create(ServiceInterface::class.java)
        apiCall.getState(obj).enqueue(object :Callback<BaseModel>{
            override fun onFailure(call: Call<BaseModel>, t: Throwable) {
                Log.e("TAG",t.printStackTrace().toString())

            }

            override fun onResponse(call: Call<BaseModel>, response: Response<BaseModel>) {
                Log.e("TAG",response.message())

                if(response.isSuccessful){
                    val mainResp = response.body()!!
                    if(mainResp.status!!){
                        state_list = mainResp.data!!
                        val customAdapter = CustomAdapter(applicationContext, mainResp.data!!)
                        state_spinner.adapter = customAdapter
                    }
                }
            }

        })
    }

    fun getCity(){

        val obj = JsonObject()
        obj.addProperty("language",SharedPref.getSharedPreferences(applicationContext,SharedPref.LANGUAGE))
        obj.addProperty("state_id",city_id)
        val apiCall = AgentServiceGenerator.create().create(ServiceInterface::class.java)
        apiCall.getCity(obj).enqueue(object :Callback<BaseModel>{
            override fun onFailure(call: Call<BaseModel>, t: Throwable) {
                Log.e("TAG",t.printStackTrace().toString())

            }

            override fun onResponse(call: Call<BaseModel>, response: Response<BaseModel>) {
                Log.e("TAG",response.message())

                if(response.isSuccessful){
                    val mainResp = response.body()!!
                    if(mainResp.status!!){

                        val customAdapter = CustomAdapter(applicationContext, mainResp.data!!)
                        city_spinner.adapter = customAdapter
                    }
                }
            }

        })
    }

    fun isEmailValid(email: String): Boolean {
        return EMAIL_REGEX.toRegex().matches(email);
    }


    fun registeruser(){
        showIndicator("Register ")

        val obj = JsonObject()
        obj.addProperty("name",et_name.text.toString())
        obj.addProperty("email",et_email.text.toString())
        obj.addProperty("password",et_pswd.text.toString())
        obj.addProperty("nationality",_id)
        obj.addProperty("phone_number",et_phone.text.toString())
        obj.addProperty("address",et_address.text.toString())
        obj.addProperty("password_confirmation",et_confpswd.text.toString())
        obj.addProperty("password",et_pswd.text.toString())
        obj.addProperty("terms_condition",et_address.text.toString())
        obj.addProperty("num_matches",et_matches.text.toString())
        obj.addProperty("token","12345")
        obj.addProperty("device_type","abdroid")
        obj.addProperty("plan_id",plan_id)
        obj.addProperty("region_id",country_id)
        obj.addProperty("state_id",state_id)
        obj.addProperty("city_id",city_id)

        val apiCall = AgentServiceGenerator.create().create(ServiceInterface::class.java)
        apiCall.registerUser(et_name.text.toString(),et_email.text.toString(),et_pswd.text.toString(),
                country_id.toString(),et_phone.text.toString(),et_address.text.toString(),et_confpswd.text.toString(),
                "1",et_matches.text.toString(),"1234","android",plan_id.toString(),
                country_id.toString(),state_id.toString(),city_id.toString()
                )
            .enqueue(object: Callback<LoginBase>{
            override fun onFailure(call: Call<LoginBase>, t: Throwable) {
                hideIndicator()
                Log.e("TAG",t.printStackTrace().toString())
            }

            override fun onResponse(call: Call<LoginBase>, response: Response<LoginBase>) {
                hideIndicator()
                val mainResp = response.body()!!
                Snackbar.make(constraint_layout, mainResp.message!!, Snackbar.LENGTH_SHORT)
                    .show()
                if(mainResp.status!!){
                    startActivity(Intent(this@SignUpActivity,LoginActivity::class.java))
                    finish()

                }

            }
        })
    }

    private fun openTerms(){
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse("http://live.cppatidar.com/theagent/en/termsofuse")
        startActivity(i)
    }
}
