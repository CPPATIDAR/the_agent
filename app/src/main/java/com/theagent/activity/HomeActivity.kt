package com.theagent.activity

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.app.ActivityCompat
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.internal.NavigationMenuView
import com.google.android.material.navigation.NavigationView
import com.theagent.R
import com.theagent.fragment.RoundedBottomSheetDialogFragment
import com.theagent.inherited_classes.BaseActivity
import com.theagent.util.SharedPref
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : BaseActivity(), RoundedBottomSheetDialogFragment.BottomSheetListnerr {

    internal lateinit var listener: BottomClick


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        super.init()
        initView()
        makeRequest()
        disableMenuScroll(nav_view)

    }

    fun initView(){

        if(!SharedPref.getSharedPreferences(applicationContext,SharedPref.USER_IMAGE).isNullOrEmpty()){
            Glide.with(applicationContext).load(SharedPref.getSharedPreferences(applicationContext,SharedPref.USER_IMAGE)).into(profile_image)
        }

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment?
        NavigationUI.setupWithNavController(bottom_navigation, navHostFragment!!.navController)
        NavigationUI.setupWithNavController(nav_view, navHostFragment!!.navController)


        img_menu.setOnClickListener { v: View? ->

            drawer_layout.openDrawer(Gravity.LEFT)
        }

        val headerView = nav_view.getHeaderView(0)
        initheaderLayout(headerView)

    }

    fun initheaderLayout(headerView:View){
        val img_nav_menu = headerView.findViewById<ImageView>(R.id.img_nav_menu)
        val ll_subs = headerView.findViewById<LinearLayout>(R.id.img_subs)
        val img_news = headerView.findViewById<ImageView>(R.id.img_news)
        val img_match = headerView.findViewById<ImageView>(R.id.img_match)
        val ll_logout = headerView.findViewById<LinearLayout>(R.id.ll_logout)
        val ll_update = headerView.findViewById<LinearLayout>(R.id.img_update)


        ll_logout.setOnClickListener { v: View? ->
            showDialog()
        }

        ll_update.setOnClickListener { v: View? ->
            drawer_layout.closeDrawer(Gravity.LEFT)
            val navController: NavController = Navigation.findNavController(this !!,R.id.nav_host_fragment)
            if(navController.getCurrentDestination()!!.getId() == R.id.homeFragment){
                navController.navigate(R.id.action_homeFragment_to_userStatsFragment)
            }else if(navController.getCurrentDestination()!!.getId() == R.id.profileFragment){
                navController.navigate(R.id.action_profileFragment_to_userStatsFragment)

            }else if(navController.getCurrentDestination()!!.getId() == R.id.inboxFragment){

            }
        }

        ll_subs.setOnClickListener { v: View? ->
            drawer_layout.closeDrawer(Gravity.LEFT)
            val navController: NavController = Navigation.findNavController(this !!,R.id.nav_host_fragment)
            if(navController.getCurrentDestination()!!.getId() == R.id.homeFragment){
                navController.navigate(R.id.action_homeFragment_to_transactionHistoryFragment)
            }else if(navController.getCurrentDestination()!!.getId() == R.id.profileFragment){
                navController.navigate(R.id.action_profileFragment_to_userStatsFragment)

            }else if(navController.getCurrentDestination()!!.getId() == R.id.inboxFragment){

            }

        }


        img_nav_menu.setOnClickListener { v: View? ->
            drawer_layout.closeDrawer(Gravity.LEFT)
        }
    }

    private fun disableMenuScroll(navView: NavigationView) {
        val navMenu = navView.getChildAt(0) as NavigationMenuView
        navMenu.layoutManager = object : LinearLayoutManager(this) {
            override fun canScrollVertically(): Boolean {
                return false
            }
        }
    }

    fun showDialog(){
        val md_dialog = MaterialAlertDialogBuilder(this)
        md_dialog.setTitle(R.string.logout_text)
            .setPositiveButton(R.string.ok,DialogInterface.OnClickListener{dialog, which ->
                SharedPref.clearSharedPreferences(applicationContext)
                startActivity(Intent(this,LandingActivity::class.java))
                finish()
            })
            .setNegativeButton(R.string.cancel, DialogInterface.OnClickListener{ dialog, which ->
            dialog.dismiss()
        })
        md_dialog.show()
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
            ),
            101
        )
    }



    fun initBottomInterface(switchInterface:BottomClick){
        listener = switchInterface
    }


    interface BottomClick {
        fun onCameraLick(isVideo:Boolean)
        fun onGalleryClick(isVideo:Boolean)
    }

    override fun cameraClick(isVideo: Boolean) {
        listener.onCameraLick(isVideo)
    }

    override fun galleryClick(isVideo: Boolean) {
        listener.onGalleryClick(isVideo)
    }

}
