package com.theagent.activity

import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK
import android.content.res.ColorStateList
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.google.android.material.snackbar.Snackbar
import com.google.gson.JsonObject
import com.theagent.R
import com.theagent.api.AgentServiceGenerator
import com.theagent.api.ServiceInterface
import com.theagent.constant.Constants
import com.theagent.fragment.AgentHomeFragment
import com.theagent.inherited_classes.BaseActivity
import com.theagent.models.LoginBase
import com.theagent.util.SharedPref
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.et_name
import kotlinx.android.synthetic.main.activity_login.et_pswd
import kotlinx.android.synthetic.main.activity_login.ti_name
import kotlinx.android.synthetic.main.activity_login.ti_pswd
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.android.synthetic.main.profile_layout.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class LoginActivity : BaseActivity() {

    val EMAIL_REGEX = "^[A-Za-z](.*)([@]{1})(.{1,})(\\.)(.{1,})";

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        super.init()

        clickListner()
    }

    fun clickListner(){

        btn_login.setOnClickListener { v: View? ->
            if(isEmailValid(et_name.text.toString())){
                ti_name.error= ""
                if(!et_pswd.text.isNullOrEmpty()){
                    loginApi()
                    if(et_pswd !=null){
                        et_pswd.hideKeyboard()
                    }else if(et_mail != null){
                        et_mail.hideKeyboard()
                    }


                }else{
                    ti_pswd.boxStrokeErrorColor = ColorStateList.valueOf(resources.getColor(R.color.red))
                    ti_pswd.error = "Please enter password"
                }
            }else{
                ti_name.boxStrokeErrorColor = ColorStateList.valueOf(resources.getColor(R.color.red))
                ti_name.error = "Please enter valid email"
            }
        }

        tv_frgtPswd.setOnClickListener { v: View? ->

        }
    }

    fun loginApi(){
        showIndicator("Login.")

        val obj = JsonObject()
        obj.addProperty("email",et_name.text.toString())
        obj.addProperty("password",et_pswd.text.toString())

        val apiCall = AgentServiceGenerator.create().create(ServiceInterface::class.java)
        apiCall.loginUser(obj).enqueue(object: Callback<LoginBase>{
            override fun onFailure(call: Call<LoginBase>, t: Throwable) {
                Log.e("TAG",t.printStackTrace().toString())
                hideIndicator()
                Snackbar.make(cl_login, "Something went wrong please try again", Snackbar.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(call: Call<LoginBase>, response: Response<LoginBase>) {
                hideIndicator()

                val mainResp = response.body()!!
                Snackbar.make(cl_login, mainResp.message!!, Snackbar.LENGTH_SHORT)
                    .show()
                if(mainResp.status!!){

                    SharedPref.setSharedPreferences(applicationContext,
                        mainResp.accessToken.toString(),SharedPref.TOKEN_DATA)
                    SharedPref.setSharedPreferences(applicationContext,mainResp.tokenType.toString()!!,SharedPref.AUTH_TYPE)
                    SharedPref.setSharedPreferences(applicationContext,
                        mainResp.basedata?.name!!,SharedPref.NAME)

                    if(mainResp.role == 0){
                        SharedPref.setSharedPreferences(applicationContext,"player",SharedPref.USER_TYPE)
                    }else{
                        SharedPref.setSharedPreferences(applicationContext,"agent",SharedPref.USER_TYPE)
                    }
                    SharedPref.setSharedPreferences(applicationContext,
                        mainResp.basedata?.email!!,SharedPref.EMAIL)
                    SharedPref.setSharedPreferences(applicationContext,mainResp.basedata?.id.toString(),SharedPref.USER_ID)

                    Constants.setHeader(SharedPref.getSharedPreferences(applicationContext,SharedPref.AUTH_TYPE),SharedPref.getSharedPreferences(applicationContext,SharedPref.TOKEN_DATA))
                    if(mainResp.role  == 0){
                        startActivity(Intent(this@LoginActivity,HomeActivity::class.java).setFlags(
                            Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or FLAG_ACTIVITY_CLEAR_TASK
                        ))
                        finish()
                    }else{
                        startActivity(Intent(this@LoginActivity,AgentActivity::class.java).setFlags(
                            Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or FLAG_ACTIVITY_CLEAR_TASK
                        ))
                        finish()
                    }
                }else{
                    Snackbar.make(cl_login, "Something went wrong please try again", Snackbar.LENGTH_SHORT)
                        .show()
                }
            }


        })
    }

    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }


    fun isEmailValid(email: String): Boolean {
            return EMAIL_REGEX.toRegex().matches(email);
    }


}
