package com.theagent.activity

import android.content.DialogInterface
import android.content.Intent
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.theagent.R
import com.theagent.util.SharedPref
import kotlinx.android.synthetic.main.activity_landing.*
import java.util.*


class LandingActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_landing)

        initListner()
    }

    fun initListner(){

        if(SharedPref.getSharedPreferences(applicationContext,SharedPref.LANGUAGE).isNullOrBlank()){
            SharedPref.setSharedPreferences(applicationContext,"0",SharedPref.LANGUAGE)
        }

        btn_login.setOnClickListener { v: View? ->
            startActivity(Intent(this,LoginActivity::class.java))
        }

        btn_sign_up.setOnClickListener { v: View? ->

            showDialog()
        }
        
        tv_en.setOnClickListener { v: View? ->
            SharedPref.setSharedPreferences(applicationContext,"0",SharedPref.LANGUAGE)
            setLocale("en")
        }

        tv_ar.setOnClickListener { v: View? ->
            SharedPref.setSharedPreferences(applicationContext,"1",SharedPref.LANGUAGE)
            setLocale("ar")
        }
    }

    fun showDialog(){
        val md_dialog = MaterialAlertDialogBuilder(this,R.style.MyThemeOverlay_MaterialComponents_MaterialAlertDialog)
        md_dialog.setTitle(R.string.select_role).
        setItems(R.array.role_array,DialogInterface.OnClickListener { dialog, pos ->
            Log.e("TAG",pos.toString())
            val intent = Intent(this,PlansActivity::class.java)
            intent.putExtra("type",pos)
            startActivity(intent)
        }).setNegativeButton(R.string.cancel,DialogInterface.OnClickListener{dialog, which ->
            dialog.dismiss()
        })
        md_dialog.show()
    }


    fun setLocale(lang: String?) {
        val myLocale = Locale(lang)
        val res = resources
        val dm = res.displayMetrics
        val conf = res.configuration
        conf.locale = myLocale
        res.updateConfiguration(conf, dm)
        val refresh = Intent(this, LandingActivity::class.java)
        finish()
        startActivity(refresh)
    }

}
