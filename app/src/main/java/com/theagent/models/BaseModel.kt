package com.theagent.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class BaseModel {

    @SerializedName("status")
    @Expose
    val status : Boolean?=false
    @SerializedName("message")
    @Expose
    val message : String?=null
    @SerializedName("data")
    @Expose
    val data : List<Data> ?=null
}