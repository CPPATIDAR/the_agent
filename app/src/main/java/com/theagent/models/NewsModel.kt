package com.theagent.models

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NewsModel(var id:Int,var title:String,var media_type:String,var short_description:String,var description:String,var media:String ):Parcelable{

}