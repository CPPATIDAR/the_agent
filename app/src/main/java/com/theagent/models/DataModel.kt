package com.theagent.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class DataModel {


    @SerializedName("id")
    @Expose
     val id: Int? = null

    @SerializedName("name")
    @Expose
     val name: String? = null

    @SerializedName("last_name")
    @Expose
     val lastName: String? = null

    @SerializedName("email")
    @Expose
     val email: String? = null

    @SerializedName("email_verified_at")
    @Expose
     val emailVerifiedAt: Any? = null

    @SerializedName("phone_number")
    @Expose
     val phoneNumber: String? = null

    @SerializedName("verification_token")
    @Expose
     val verificationToken: Any? = null



    @SerializedName("bio_graphy")
    @Expose
     val bioGraphy: Any? = null

    @SerializedName("address")
    @Expose
     val address: String? = null

    @SerializedName("squad_number")
    @Expose
     val squadNumber: Any? = null

    @SerializedName("position_id")
    @Expose
     val positionId: Any? = null

    @SerializedName("date_of_birth")
    @Expose
     val dateOfBirth: String? = null

    @SerializedName("height")
    @Expose
     val height: String? = null

    @SerializedName("type")
    @Expose
     val type: Any? = null

    @SerializedName("weight")
    @Expose
     val weight: String? = null

    @SerializedName("birth_place")
    @Expose
     val birthPlace: Any? = null

    @SerializedName("nationality")
    @Expose
     val nationality: String? = null


    @SerializedName("blood_group")
    @Expose
     val bloodGroup: String? = null

    @SerializedName("prev_clubs")
    @Expose
     val prevClubs: Any? = null

    @SerializedName("num_matches")
    @Expose
     val numMatches: Int? = null

    @SerializedName("is_active")
    @Expose
     val isActive: Int? = null

    @SerializedName("token")
    @Expose
     val token: Any? = null

    @SerializedName("device_type")
    @Expose
     val deviceType: Any? = null

    @SerializedName("terms_condition")
    @Expose
     val termsCondition: Int? = null

    @SerializedName("deleted_at")
    @Expose
     val deletedAt: Any? = null

    @SerializedName("created_at")
    @Expose
     val createdAt: String? = null

    @SerializedName("updated_at")
    @Expose
     val updatedAt: String? = null

    @SerializedName("roles")
    @Expose
     val roles: List<Role>? = null

    @SerializedName("price")
    @Expose
    val price: String? = null

    @SerializedName("expiry_date")
    @Expose
    val expiry_date: String? = null

    @SerializedName("first_name")
    @Expose
    val first_name: String? = null

    @SerializedName("goals")
    @Expose
    val goals: String? = null
    @SerializedName("player_of_the_match")
    @Expose
    val player_of_the_match: String? = null
    @SerializedName("red_cards")
    @Expose
    val red_cards: String? = null
    @SerializedName("yellow_cards")
    @Expose
    val yellow_cards: String? = null

    @SerializedName("transaction_date")
    @Expose
    val transaction_date: String? = null
//
//    @SerializedName("profile_picture")
//    @Expose
//    val profile_picture: List<Video>? = null


}