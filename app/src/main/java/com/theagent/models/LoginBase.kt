package com.theagent.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LoginBase {

    @SerializedName("status")
    @Expose
    val status: Boolean? = false

    @SerializedName("message")
    @Expose
    val message: String? = null

    @SerializedName("role")
    @Expose
    val role:Int? = null

    @SerializedName("data")
    @Expose
     val basedata: DataModel? = null

    @SerializedName("access_token")
    @Expose
     val accessToken: String? = null

    @SerializedName("token_type")
    @Expose
     val tokenType: String? = null

}