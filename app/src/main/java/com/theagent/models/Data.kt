package com.theagent.models

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class Data : Parcelable {


    @SerializedName("id")
    @Expose
    val id : Int?= null
    @SerializedName("name")
    @Expose
    val name : String?=null
    @SerializedName("icon")
    @Expose
    val icon : String?=null
    @SerializedName("color")
    @Expose
    val color : String?=null
    @SerializedName("sub_title")
    @Expose
    val sub_title : String?=null
    @SerializedName("price")
    @Expose
    val price : String?=null
    @SerializedName("description")
    @Expose
    val description : List<String> ?=null
    @SerializedName("button_text")
    @Expose
    val button_text : String?=null
    @SerializedName("rating")
    @Expose
    val rating : Int?=null
    @SerializedName("type")
    @Expose
    val type : Int?=null
    @SerializedName("is_active")
    @Expose
    val is_active : Int?=null
    @SerializedName("is_free")
    @Expose
    val is_free : Int?=null
    @SerializedName("remember_token")
    @Expose
    val remember_token : String?=null
    @SerializedName("created_at")
    @Expose
    val created_at : String?=null
    @SerializedName("updated_at")
    @Expose
    val updated_at : String ?=null
    @SerializedName("title")
    @Expose
    val title : String ?=null
    @SerializedName("media_type")
    @Expose
    val media_type : String ?=null
    @SerializedName("short_description")
    @Expose
    val short_description : String ?=null
    @SerializedName("media")
    @Expose
    val media : String ?=null

    @SerializedName("short_name1")
    @Expose
    val short_name1 : String ?=null
    @SerializedName("short_name2")
    @Expose
    val short_name2 : String ?=null
    @SerializedName("team1")
    @Expose
    val team1 : String ?=null
    @SerializedName("team2")
    @Expose
    val team2 : String ?=null
    @SerializedName("country")
    @Expose
    val country : String ?=null
    @SerializedName("date")
    @Expose
    val date : String ?=null
    @SerializedName("time")
    @Expose
    val time : String ?=null
    @SerializedName("number_of_players")
    @Expose
    val number_of_players : String ?=null
    @SerializedName("is_join_match")
    @Expose
    var is_join_match : String ?=null
    @SerializedName("profile_picture")
    @Expose
    var profile_picture : String ?=null
    @SerializedName("last_name")
    @Expose
    var last_name : String ?=null
    @SerializedName("position")
    @Expose
    var position : String ?=null
    @SerializedName("chat_id")
    @Expose
    var chat_id : String ?=null
    @SerializedName("from")
    @Expose
    var from : String ?=null
    @SerializedName("to")
    @Expose
    var to : String ?=null
    @SerializedName("message")
    @Expose
    var message : String ?=null
    @SerializedName("user_id")
    @Expose
    var user_id : String ?=null
}