package com.theagent.models

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class Pivot {

    @SerializedName("model_id")
    @Expose
     val modelId: Int? = null

    @SerializedName("role_id")
    @Expose
     val roleId: Int? = null

    @SerializedName("model_type")
    @Expose
     val modelType: String? = null
}