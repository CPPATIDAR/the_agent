package com.theagent.models

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class Role {


    @SerializedName("id")
    @Expose
     val id: Int? = null

    @SerializedName("name")
    @Expose
     val name: String? = null

    @SerializedName("guard_name")
    @Expose
     val guardName: String? = null

    @SerializedName("is_active")
    @Expose
     val isActive: Int? = null

    @SerializedName("created_at")
    @Expose
     val createdAt: String? = null

    @SerializedName("updated_at")
    @Expose
     val updatedAt: String? = null

    @SerializedName("pivot")
    @Expose
     val pivot: Pivot? = null
}