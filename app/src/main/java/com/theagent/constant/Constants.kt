package com.theagent.constant


class Constants {

    companion object{

        var headerData:String?= null

        const val GET_PLANS = "theagent/api/getPlans"
        const val GET_REGION ="theagent/api/getRegions"
        const val GET_COUNTRY = "theagent/api/getCountries"
        const val GET_STATE = "theagent/api/getStates"
        const val GET_CITY = "theagent/api/getCities"
        const val LOGIN ="theagent/api/login"
        const val SIGNUP ="theagent/api/register"
        const val NEWS ="theagent/api/getNews"
        const val CURRENT_PLAN ="theagent/api/getCurrentUserPlan"
        const val UPCOMING_MATCH ="theagent/api/getUpComingMatches"
        const val JOIN_MATCH ="theagent/api/userJoinMatch"
        const val GET_PROFILE ="theagent/api/getUserProfile"
        const val UPDATE_PROFILE ="theagent/api/updateUserProfile"
        const val PLAYER_STATS = "theagent/api/userHasMatch"
        const val UPDATE_STATS="theagent/api/updateMatchperformance"
        const val PLAYER_PERFOMMANCE ="theagent/api/getMatchPerformance"
        const val TRANSACTION_HISTORY ="theagent/api/getTransactionHistory"
        const val GET_STATICS = "theagent/api/getStatistics"
        const val REMOVE_MEDIA = "theagent/api/removeMedia"
        const val SEARCH_PLAYER = "theagent/api/getPlayers"
        const val SAVE_CHAT = "theagent/api/saveChatData"
        const val GET_CHAT_HISTORY = "theagent/api/getChatHistory"
        const val CHAT_REQUEST = "theagent/api/sendChatRequest"
        const val IS_FAV = "theagent/api/isFavourite"
        const val GET_FAV = "theagent/api/getFavourite"
        const val GET_CHAT_REQUEST ="theagent/api/getChatRequest"

        fun setHeader(header_type:String,header_token:String){

           this.headerData = header_type+" "+header_token
        }

        fun getHeader(): String? {
            return this.headerData
        }

    }
}