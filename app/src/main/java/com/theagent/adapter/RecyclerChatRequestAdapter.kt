package com.theagent.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.theagent.R
import com.theagent.models.Data

class RecyclerChatRequestAdapter(val mContext: Context, val list: List<Data>?, val listener: (Data) -> Unit): RecyclerView.Adapter<RecyclerChatRequestAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerChatRequestAdapter.MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recycler_request_layout,parent,false)

        return RecyclerChatRequestAdapter.MyViewHolder(view)
    }

    override fun getItemCount(): Int {
      return list!!.size
    }

    override fun onBindViewHolder(holder: RecyclerChatRequestAdapter.MyViewHolder, position: Int) {
        Glide.with(mContext).load(list?.get(position)!!.profile_picture).into(holder.img_user)
        holder.user_name.setText(list[position].name + " "+list[position].last_name)
        holder.user_message.setText(list[position].message)

        holder.cardRequest.setOnClickListener { v: View? ->
            this.listener(list[position])
        }
    }

    class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        var img_user = itemView.findViewById<ImageView>(R.id.img_user)
        var user_name = itemView.findViewById<TextView>(R.id.tv_chat_name)
        var user_message = itemView.findViewById<TextView>(R.id.tv_player_message)
        var cardRequest = itemView.findViewById<CardView>(R.id.card_request)
    }

}