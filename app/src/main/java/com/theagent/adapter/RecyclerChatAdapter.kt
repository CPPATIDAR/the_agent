package com.theagent.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.theagent.R
import com.theagent.models.ChatModel
import com.theagent.util.SharedPref
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_home.*

class RecyclerChatAdapter(val mContext: Context, var list: MutableList<ChatModel>?): RecyclerView.Adapter<RecyclerChatAdapter.MyViewHolder>() {


    companion object {
        private const val SENT = 0
        private const val RECEIVED = 1
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerChatAdapter.MyViewHolder {
        val view = when (viewType) {
            SENT -> {
                LayoutInflater.from(parent.context).inflate(R.layout.chat_sent_layout, parent, false)
            }
            else -> {
                LayoutInflater.from(parent.context).inflate(R.layout.chat_recvd_layout, parent, false)
            }
        }
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
       return list!!.size
    }

    override fun onBindViewHolder(holder: RecyclerChatAdapter.MyViewHolder, position: Int) {
        holder.tv_message.setText(list!![position].message)
        holder.tv_time.setText(list!![position].time)
        Log.e("TAG",list!![position].image)
        if(list!![position].type == 1){
            if(!list!![position].image.isNullOrEmpty()){
                Glide.with(mContext).load(list!![position].image)
                    .into(holder.image_message_profile)
            }
        }else if(list!![position].type == 0){
            if(!list!![position].image.isNullOrEmpty()){
                Glide.with(mContext).load(list!![position].image)
                    .into(holder.user_image)
            }
        }

    }

    class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val tv_message = itemView.findViewById<TextView>(R.id.text_message_body)
        val tv_time = itemView.findViewById<TextView>(R.id.text_message_time)
        val user_image = itemView.findViewById<CircleImageView>(R.id.image_message_user)
        val image_message_profile = itemView.findViewById<ImageView>(R.id.image_message_profile)

    }

    override fun getItemViewType(position: Int): Int {
        return if (list!![position].type == 0) {
            SENT
        } else {
            RECEIVED
        }
    }

    fun appendMessage(message: ChatModel) {
        Log.e("TAG","chat message"+message.message)
        this.list!!.add(message)
        notifyItemInserted(this.list!!.size - 1)
    }

    fun appendlast(message: ChatModel){
        this.list!!.add(0,message)
        notifyItemInserted(this.list!!.size - 1)
    }
}