package com.theagent.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textview.MaterialTextView
import com.theagent.R
import com.theagent.fragment.HomeFragment
import com.theagent.models.Data

class RecyclerMatcheAdapter(val mContext: Context, val list: List<Data>?,val listener: (Int,Int) -> Unit): RecyclerView.Adapter<RecyclerMatcheAdapter.MyViewholder>() {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerMatcheAdapter.MyViewholder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recycler_match_layout,parent,false);

        return MyViewholder(view)
    }

    override fun getItemCount(): Int {
       return list!!.size
    }


    override fun onBindViewHolder(holder: MyViewholder, position: Int) {
        holder.tv_match.text = list!!.get(position).short_name1
        holder.tv_match_2.text = list!!.get(position).short_name2
        holder.tv_team.text = list!!.get(position).team1
        holder.tv_team_2.text = list.get(position).team2
        holder.tv_date.text = list.get(position).date
        holder.tv_time.text = list.get(position).time

        if(!list.get(position).is_join_match.isNullOrEmpty()){
            if(list.get(position).is_join_match!!.toInt() == 0){
                holder.tv_join.visibility = View.VISIBLE
            }else if(list.get(position).is_join_match!!.toInt() == 2){
                holder.tv_join.visibility = View.VISIBLE
                holder.tv_join.setText("Joined")
            }else if(list.get(position).is_join_match!!.toInt() == 1){
                holder.tv_join.visibility = View.VISIBLE
                holder.tv_join.setText("Request Send")
            }
        }else{
            holder.tv_join.visibility = View.GONE
        }

        holder.card_match.setOnClickListener { v: View? ->
            if(list.get(position).is_join_match.isNullOrEmpty()){
                if(list.get(position).is_join_match!!.toInt() == 0){
                    listener(list.get(position).id!!,position)
                }
            }
        }

        holder.tv_join.setOnClickListener { v: View? ->
            listener(list.get(position).id!!,position)
        }
    }

    class MyViewholder(itemView: View): RecyclerView.ViewHolder(itemView){
        var tv_match = itemView.findViewById<TextView>(R.id.tv_match)
        var tv_match_2 = itemView.findViewById<TextView>(R.id.tv_match_2)
        var tv_team = itemView.findViewById<TextView>(R.id.tv_team)
        var tv_team_2 = itemView.findViewById<TextView>(R.id.tv_team_2)
        var tv_date = itemView.findViewById<TextView>(R.id.tv_date)
        var tv_time = itemView.findViewById<TextView>(R.id.tv_time)
        var tv_join = itemView.findViewById<Button>(R.id.btn_join)
        var card_match = itemView.findViewById<CardView>(R.id.card_match)
    }

    fun disableBtn(position: Int){

    }


}