package com.theagent.adapter

import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.theagent.R
import com.theagent.models.NewsModel

class RecyclerNewsAdapter(val mContext: Context, val list: List<NewsModel>?,val listener: (NewsModel) -> Unit): RecyclerView.Adapter<RecyclerNewsAdapter.MyViewholder>() {

    private lateinit var player:SimpleExoPlayer

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerNewsAdapter.MyViewholder {
      val view = LayoutInflater.from(parent.context).inflate(R.layout.recycler_news_layout,parent,false)

        return MyViewholder(view)
    }

    override fun getItemCount(): Int {
      return list!!.size
    }

    override fun onBindViewHolder(holder: RecyclerNewsAdapter.MyViewholder, position: Int) {
        if(list!!.get(position).media_type.equals("image")){
            Glide.with(mContext).load(list!![position].media).into(holder.img_news)
        }else{
            Glide.with(mContext).load(list!![position].media)
                .into(holder.img_news)
            holder.img_play.visibility = View.VISIBLE
        }

        holder.card_news.setOnClickListener { v: View? ->
            listener(list[position])
        }

        holder.img_play.setOnClickListener { v: View? ->
            holder.video_view.visibility = View.VISIBLE
            holder.img_news.visibility = View.GONE
            holder.img_play.visibility = View.GONE


          player = ExoPlayerFactory.newSimpleInstance(mContext)
            holder.video_view.setPlayer(player)
            val uri = Uri.parse(list!![position].media)
            val mediaSource = buildMediaSource(uri)
            player.prepare(mediaSource, false, false)
            holder.video_view.setShutterBackgroundColor(Color.TRANSPARENT)
            player.playWhenReady = true
        }

        holder.tv_news.text=android.text.Html.fromHtml(list!![position].title).toString()
    }

    class MyViewholder(itemView: View): RecyclerView.ViewHolder(itemView){
        val tv_news = itemView.findViewById<TextView>(R.id.tv_news)
        val img_news = itemView.findViewById<ImageView>(R.id.img_news)
        val img_play =  itemView.findViewById<ImageView>(R.id.img_play)
        val video_view = itemView.findViewById<PlayerView>(R.id.video_view)
        val card_news = itemView.findViewById<CardView>(R.id.card_news)
    }

    private fun buildMediaSource(uri: Uri): MediaSource? {
        val dataSourceFactory: DataSource.Factory =
            DefaultDataSourceFactory(mContext, "exoplayer-codelab")
        return ProgressiveMediaSource.Factory(dataSourceFactory)
            .createMediaSource(uri)
    }

    fun stopPlayer(){
        Log.e("TAG","player")
        player.stop()
    }


}