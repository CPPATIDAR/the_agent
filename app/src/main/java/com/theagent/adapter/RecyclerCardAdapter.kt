package com.theagent.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.recyclerview.widget.RecyclerView
import com.theagent.R
import com.theagent.models.Data

class RecyclerCardAdapter(val mContext: Context, val list: List<Data>?,val listener: (Boolean,String) -> Unit): RecyclerView.Adapter<RecyclerCardAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerCardAdapter.MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recycler_checkboc_layout,parent,false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: RecyclerCardAdapter.MyViewHolder, position: Int) {
        holder.checkBox.setText(list?.get(position)!!.name)
        holder.checkBox.setOnCheckedChangeListener { buttonView, isChecked ->
            listener(isChecked, list[position].id.toString())
        }
    }

    class MyViewHolder(itemView:View):RecyclerView.ViewHolder(itemView){
        val checkBox = itemView.findViewById<CheckBox>(R.id.checkbox_rc)
    }
}