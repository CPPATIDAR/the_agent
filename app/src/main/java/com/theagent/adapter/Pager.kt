package com.theagent.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import com.theagent.R
import com.theagent.activity.SignUpActivity
import com.theagent.models.Data

class Pager(private val mContext:Context, private val list: List<Data>?) :PagerAdapter() {

    lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getCount(): Int {
        return list!!.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val inflater = LayoutInflater.from(mContext)
        val layout = inflater.inflate(R.layout.pager_plan_layout,container,false) as ViewGroup

        val tvplan = layout.findViewById<TextView>(R.id.tv_plan)
        val tvamt = layout.findViewById<TextView>(R.id.tv_amt)
        val recycler_pager = layout.findViewById<RecyclerView>(R.id.recycler_pager)
        val btn_plan = layout.findViewById<Button>(R.id.btn_plan)


        tvplan.setText(list!!.get(position).name)
        tvamt.setText(list!!.get(position).price.toString())
        viewManager = LinearLayoutManager(mContext)
        viewAdapter = RecyclerAdapter(mContext !!, list!!.get(position).description!!)
        recycler_pager.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }

        btn_plan.setOnClickListener { v:View? ->
            val intent = Intent(mContext,SignUpActivity::class.java)
            intent.putExtra("plan_id",list!!.get(position).id.toString())
            mContext.startActivity(intent)
        }

        container.addView(layout)
        return layout
    }

    override fun destroyItem(container: View, position: Int, `object`: Any) {
       // container.removeOnLayoutChangeListener(`object` as View)
    }
}