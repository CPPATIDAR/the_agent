package com.theagent.adapter

import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.theagent.R
import com.theagent.models.Video

class RecyclerVideoAdapter(val mContext: Context, val list: List<Video>?,val listener: (String,Int) -> Unit): RecyclerView.Adapter<RecyclerVideoAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerVideoAdapter.MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recycler_video_layout,parent,false)

        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
      return list!!.size
    }

    override fun onBindViewHolder(holder: RecyclerVideoAdapter.MyViewHolder, position: Int) {
        Glide.with(mContext).load(list!!.get(position).url)
            .into(holder.img_news)
        holder.img_play.visibility = View.VISIBLE

        holder.img_play.setOnClickListener { v: View? ->
            holder.video_view.visibility = View.VISIBLE
            holder.img_news.visibility = View.GONE
            holder.img_play.visibility = View.GONE


            val player = ExoPlayerFactory.newSimpleInstance(mContext)
            holder.video_view.setPlayer(player)
            val uri = Uri.parse(list!!.get(position).url)
            val mediaSource = buildMediaSource(uri)
            player.prepare(mediaSource, false, false)
            holder.video_view.setShutterBackgroundColor(Color.TRANSPARENT)
            player.playWhenReady = true
        }

        holder.img_delete.setOnClickListener { v: View? ->
            this.listener(list[position].id.toString(),position)
        }
    }


    class MyViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {
        var img_news = itemView.findViewById<ImageView>(R.id.img_news)
        val img_play =  itemView.findViewById<ImageView>(R.id.img_play)
        var video_view = itemView.findViewById<PlayerView>(R.id.video_view)
        var img_delete = itemView.findViewById<ImageView>(R.id.img_delete)
    }

    private fun buildMediaSource(uri: Uri): MediaSource? {
        val dataSourceFactory: DataSource.Factory =
            DefaultDataSourceFactory(mContext, "exoplayer-codelab")
        return ProgressiveMediaSource.Factory(dataSourceFactory)
            .createMediaSource(uri)
    }
}