package com.theagent.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.theagent.R
import com.theagent.models.Data
import kotlinx.android.synthetic.main.playerrecycler_layout.view.*

class RecyclerSearchAdapter(val mContext:Context,val list:List<Data>,val listener: (Data) -> Unit): RecyclerView.Adapter<RecyclerSearchAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerSearchAdapter.MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.playerrecycler_layout,parent,false)

        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
       return list.size
    }

    override fun onBindViewHolder(holder: RecyclerSearchAdapter.MyViewHolder, position: Int) {
        Glide.with(mContext).load(list!![position].profile_picture)
            .into(holder.imagePlayer)

        holder.tv_name.setText(list[position].name + " "+list[position].last_name)

        holder.card_player.setOnClickListener { v: View? ->
            this.listener(list[position])
        }
    }
    class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        var imagePlayer = itemView.findViewById<ImageView>(R.id.img_player)
        var tv_name = itemView.findViewById<TextView>(R.id.tv_player_name)
        var card_player = itemView.findViewById<CardView>(R.id.card_player)
    }

}