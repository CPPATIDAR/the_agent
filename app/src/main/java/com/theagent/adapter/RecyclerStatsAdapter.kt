package com.theagent.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.theagent.R

class RecyclerStatsAdapter(val mContext:Context):
    RecyclerView.Adapter<RecyclerStatsAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerStatsAdapter.MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recycler_news_layout,parent,false);

        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
      return 2
    }

    override fun onBindViewHolder(holder: RecyclerStatsAdapter.MyViewHolder, position: Int) {

    }

    class MyViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {

    }
}