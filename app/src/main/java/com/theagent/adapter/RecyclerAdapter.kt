package com.theagent.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.theagent.R

class RecyclerAdapter (val mContext: Context, val list:List<String>): RecyclerView.Adapter<RecyclerAdapter.MyViewholder>() {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerAdapter.MyViewholder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.recycler_layout,parent,false)
        return MyViewholder(view)
    }

    override fun getItemCount(): Int {
     return  list.size
    }

    override fun onBindViewHolder(holder: RecyclerAdapter.MyViewholder, position: Int) {
        holder.tv_desc.setText(list.get(position))
    }

    class MyViewholder(itemView: View): RecyclerView.ViewHolder(itemView){
        var tv_desc = itemView.findViewById<TextView>(R.id.tv_desc)
    }
}