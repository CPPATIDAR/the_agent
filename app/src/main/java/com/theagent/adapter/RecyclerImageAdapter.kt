package com.theagent.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.theagent.R
import com.theagent.models.Video

class RecyclerImageAdapter(val mContext:Context,val list: List<Video>?,val listener: (String,Int) -> Unit): RecyclerView.Adapter<RecyclerImageAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerImageAdapter.MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recycler_image_layout,parent,false);

        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
       return list!!.size
    }

    override fun onBindViewHolder(holder: RecyclerImageAdapter.MyViewHolder, position: Int) {
        Glide.with(mContext).load(list!!.get(position).url)
            .into(holder.player_image)

        holder.img_delete.setOnClickListener { v: View? ->
            this.listener(list[position].id.toString(),position)
        }
    }

    class MyViewHolder(itemView:View):RecyclerView.ViewHolder(itemView) {
        val player_image = itemView.findViewById<ImageView>(R.id.img_player)
        val img_delete = itemView.findViewById<ImageView>(R.id.img_delete)
    }
}