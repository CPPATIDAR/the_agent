package com.theagent.fragment

import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.theagent.R

class RoundedBottomSheetDialogFragment: BottomSheetDialogFragment() {

    override fun getTheme(): Int = R.style.BottomSheetDialogTheme

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog = BottomSheetDialog(requireContext(), theme)

    private lateinit var ll_camera:LinearLayout
    private lateinit var ll_gallery:LinearLayout
    var listner: BottomSheetListnerr? = null
    var isVideo:Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.bottom_layout,container,false)

        initView(view)
        clickListner()

        val mArgs = arguments
        isVideo = mArgs!!.getBoolean("isVideo")

        return view
    }

    fun initView(view:View){
        ll_camera = view.findViewById(R.id.llcamera)
        ll_gallery = view.findViewById(R.id.ll_gallery)
    }

    fun clickListner(){
        ll_camera.setOnClickListener { v: View? ->
            listner!!.cameraClick(isVideo)
        }

        ll_gallery.setOnClickListener { v: View? ->
            listner!!.galleryClick(isVideo)
        }
    }

    public interface BottomSheetListnerr{
        fun cameraClick(isVideo:Boolean)
        fun galleryClick(isVideo:Boolean)
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)

        try {
            listner = activity as BottomSheetListnerr
        } catch (e: ClassCastException) {
            throw ClassCastException(
                "$activity must implement BottomSheetListnerr"
            )
        }
    }
}