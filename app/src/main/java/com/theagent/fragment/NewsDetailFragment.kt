package com.theagent.fragment

import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.theagent.R
import com.theagent.inherited_classes.BaseFragment
import com.theagent.models.NewsModel

class NewsDetailFragment: BaseFragment() {

    private lateinit var player:SimpleExoPlayer

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.news_layout,container,false)

        super.init(view)
        initView(view)
        return view
    }

    private fun initView(view:View){
        player = ExoPlayerFactory.newSimpleInstance(context)
       val newsData = arguments!!.getParcelable<NewsModel>("data")
        Log.e("TAG",newsData!!.description)

        val tv_news = view.findViewById<TextView>(R.id.tv_news_title)
        val tv_news_long = view.findViewById<TextView>(R.id.tv_news_short)
        val tv_news_full = view.findViewById<TextView>(R.id.tv_news_full)
        val img_news = view.findViewById<ImageView>(R.id.img_news)
        val img_play =  view.findViewById<ImageView>(R.id.img_play)
        val video_view = view.findViewById<PlayerView>(R.id.video_view)

        if(newsData.media_type.equals("image")){
            Glide.with(context!!).load(newsData.media).into(img_news)
        }else{
            Glide.with(context!!).load(newsData.media)
                .into(img_news)
            img_play.visibility = View.VISIBLE
        }

        img_play.setOnClickListener { v: View? ->
            video_view.visibility = View.VISIBLE
            img_news.visibility = View.GONE
            img_play.visibility = View.GONE



            video_view.setPlayer(player)
            val uri = Uri.parse(newsData.media)
            val mediaSource = buildMediaSource(uri)
            player.prepare(mediaSource, false, false)
            video_view.setShutterBackgroundColor(Color.TRANSPARENT)
            player.playWhenReady = true
        }
        tv_news.text=android.text.Html.fromHtml(newsData.title).toString()
        tv_news_long.text=android.text.Html.fromHtml(newsData.short_description).toString()
        tv_news_full.text=android.text.Html.fromHtml(newsData.description).toString()


    }

    private fun buildMediaSource(uri: Uri): MediaSource? {
        val dataSourceFactory: DataSource.Factory =
            DefaultDataSourceFactory(context, "exoplayer-codelab")
        return ProgressiveMediaSource.Factory(dataSourceFactory)
            .createMediaSource(uri)
    }

    override fun onDestroy() {
        super.onDestroy()
        if(player != null){
            player.stop()
        }

    }
}