package com.theagent.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ScrollView
import android.widget.TextView
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.google.gson.JsonObject
import com.theagent.R
import com.theagent.adapter.Pager
import com.theagent.api.AgentServiceGenerator
import com.theagent.api.ServiceInterface
import com.theagent.inherited_classes.BaseFragment
import com.theagent.models.BaseModel
import com.theagent.models.LoginBase
import com.theagent.util.SharedPref
import kotlinx.android.synthetic.main.activity_plans.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TransactionHistoryFragment:BaseFragment() {

    private lateinit var tabsTransaction:TabLayout
    private lateinit var pagerTransaction:ViewPager
    private lateinit var nestedScroll:ScrollView
    private lateinit var tvPlan:TextView
    private lateinit var tvTransactionDt:TextView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.transaction_layout,container,false)
        super.init(view)
        initView(view)
        getPlans(0)
        return view
    }

    private fun initView(view:View){
        tabsTransaction = view.findViewById(R.id.tabs_transaction)
        pagerTransaction = view.findViewById(R.id.pager_transaction)
        nestedScroll = view.findViewById(R.id.scroll)
        tvPlan = view.findViewById(R.id.tv_plan)
        tvTransactionDt = view.findViewById(R.id.tv_transdt)


        nestedScroll.isFillViewport = true
    }

     private fun getPlans(type:Int){
        showIndicator("Fetching Plans")
        val obj = JsonObject()
        obj.addProperty("type",type)
        obj.addProperty("lan_type", SharedPref.getSharedPreferences(context!!, SharedPref.LANGUAGE))
        val apiCall = AgentServiceGenerator.create().create(ServiceInterface::class.java)
        apiCall.getPlans(obj).enqueue(object: Callback<BaseModel> {
            override fun onFailure(call: Call<BaseModel>, t: Throwable) {
                Log.e("TAG",t.localizedMessage)
                getHistory()
            }

            override fun onResponse(call: Call<BaseModel>, response: Response<BaseModel>) {
               getHistory()
                val mainResp = response.body()!!
                Log.e("TAG",mainResp.data!!.size.toString())
                if(mainResp.status!!){
                    setUppageAdapter(mainResp)
                }
            }

        })
    }

    fun setUppageAdapter(mainResp: BaseModel){
        tabsTransaction.setupWithViewPager(pagerTransaction)
        pagerTransaction.adapter = Pager(context!!,mainResp.data)
        if(mainResp.data!!.isNotEmpty()){
            for (pos in 0 until mainResp.data.size){
                tabsTransaction.getTabAt(pos)!!.setText(mainResp.data.get(pos).name)
            }

        }
        pagerTransaction.addOnPageChangeListener(object: ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                tabsTransaction.getTabAt(position)!!.setText(mainResp.data!!.get(position).name)
            }

        })
    }

    private fun getHistory(){
        val obj = JsonObject()
        obj.addProperty("language", "0")
        val apiCall = AgentServiceGenerator.retrofitWithHeader().create(ServiceInterface::class.java)
        apiCall.getTransaction(obj).enqueue(object :Callback<LoginBase>{
            override fun onFailure(call: Call<LoginBase>, t: Throwable) {
                hideIndicator()
                Log.e("TAG",t.message)
            }

            override fun onResponse(call: Call<LoginBase>, response: Response<LoginBase>) {
                hideIndicator()
                val mainResp = response.body()

                if(mainResp!!.status!!){
                    Log.e("TAG",mainResp!!.message)
                    tvPlan.setText(mainResp.basedata!!.name)
                    tvTransactionDt.setText(mainResp.basedata.transaction_date)

                }
            }

        })
    }
}