package com.theagent.fragment

import android.app.DatePickerDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.google.gson.JsonObject
import com.theagent.R
import com.theagent.adapter.RecyclerCardAdapter
import com.theagent.adapter.RecyclerChatAdapter
import com.theagent.api.AgentServiceGenerator
import com.theagent.api.ServiceInterface
import com.theagent.inherited_classes.BaseFragment
import com.theagent.models.BaseModel
import com.theagent.models.Data
import com.theagent.models.LoginBase
import com.theagent.util.SharedPref
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class UpdateStatsFragment: BaseFragment() {

    private lateinit var btn_save:Button
    private lateinit var et_date:EditText
    private lateinit var et_goal:EditText

    private var match_id: String? = null
    private var player:Int = 0
    private var redCard:Int = 0
    private var yellowCard:Int = 0
    private lateinit var apiCall:ServiceInterface
    private lateinit var recycler_checkbox:RecyclerView
    private lateinit var cardAdapter:RecyclerCardAdapter
    var listCheck:MutableList<String> = ArrayList()
    var list:MutableList<Data> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.updatestats_layout,container,false)

        super.init(view)
        initView(view)
        getStats()
        clickListner()
        GlobalScope.launch (Dispatchers.Main) {   getCards() }
        return view
    }

    private fun initView(view:View){
        apiCall = AgentServiceGenerator.retrofitWithHeader().create(ServiceInterface::class.java)

        Log.e("TAG", arguments!!.getString("team1"))
        val tv_match = view.findViewById<TextView>(R.id.tv_match)
        val tv_team = view.findViewById<TextView>(R.id.tv_team)
        val tv_match_2 = view.findViewById<TextView>(R.id.tv_match_2)
        val tv_team_2 = view.findViewById<TextView>(R.id.tv_team_2)
        val btn_dt = view.findViewById<MaterialButton>(R.id.btn_dt)

        btn_save = view.findViewById(R.id.btn_save)
        et_date = view.findViewById(R.id.et_date)
        et_goal = view.findViewById(R.id.et_goal)
        recycler_checkbox = view.findViewById(R.id.recycler_checkbox)

        tv_match.setText(arguments!!.getString("short_name1"))
        tv_match_2.setText(arguments!!.getString("short_name2"))
        tv_team.setText(arguments!!.getString("team1"))
        tv_team_2.setText(arguments!!.getString("team2"))
        btn_dt.setText(arguments!!.getString("date"))
        et_date.setText(arguments!!.getString("date"))
        match_id = arguments!!.getString("match_id")


    }

    private fun clickListner(){
        btn_save.setOnClickListener { v: View? ->
            updateStats()
        }

        et_date.setOnClickListener { v: View? ->
            showCalendar()
        }


    }

    fun showCalendar(){
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)


        val dpd = DatePickerDialog(context!!, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

            Log.e("DTE",dayOfMonth.toString()  +" "+monthOfYear + " "+year)
            val month = monthOfYear+1
            et_date.setText("")
            et_date.setText(year.toString()+"-"+month+"-"+dayOfMonth)

        }, year, month, day
        )

        dpd.show()
    }

    private fun updateStats(){
        showIndicator(context!!.getString(R.string.plswait))
        var hashmap :HashMap<String,String> = HashMap()
        for(key in listCheck){
            var temp =  " statistic["+key!!+"]"

            hashmap.put(temp,"1")
        }

        apiCall.updateSatus(match_id!!,et_goal.text.toString(),
            hashmap).enqueue(object :Callback<BaseModel>{
            override fun onFailure(call: Call<BaseModel>, t: Throwable) {
                Log.e("TAG",t.message)
                hideIndicator()

            }

            override fun onResponse(call: Call<BaseModel>, response: Response<BaseModel>) {
                val mainResp = response.body()
                hideIndicator()
                Toast.makeText(context,mainResp!!.message,Toast.LENGTH_SHORT).show()
                if(mainResp!!.status!!){
                    Toast.makeText(context,mainResp.message,Toast.LENGTH_SHORT)
                }
            }

        })
    }

    private fun getStats(){
        showIndicator(context!!.getString(R.string.plswait))
        val obj = JsonObject()
        obj.addProperty("match_id", match_id)
        apiCall.getStats(obj).enqueue(object :Callback<LoginBase>{
            override fun onFailure(call: Call<LoginBase>, t: Throwable) {
                Log.e("TAG",t.message)
                hideIndicator()
            }

            override fun onResponse(call: Call<LoginBase>, response: Response<LoginBase>) {
                hideIndicator()
                val mainResp = response.body()
                if(mainResp!!.status!!){
                    et_goal.setText(mainResp.basedata!!.goals)


                }
            }

        })
    }

    suspend fun getCards(){
        val obj = JsonObject()
        obj.addProperty("language",SharedPref.getSharedPreferences(context!!,SharedPref.LANGUAGE))
        val response = apiCall.getStatics("0")
        val viewManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL,false)
        list = response.data as MutableList<Data>
        cardAdapter = RecyclerCardAdapter(context !!, list){isCheck,id ->
        Log.e("TAG",isCheck.toString() + " "+id)
            if(isCheck){
                listCheck!!.add(id)
            }else{
                if(listCheck!!.contains(id)){
                    listCheck.remove(id)
                }
            }

        }
        recycler_checkbox.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = cardAdapter
        }
    }


}