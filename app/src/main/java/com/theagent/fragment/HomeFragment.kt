package com.theagent.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textview.MaterialTextView
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.theagent.R
import com.theagent.adapter.RecyclerMatcheAdapter
import com.theagent.adapter.RecyclerNewsAdapter
import com.theagent.api.AgentServiceGenerator
import com.theagent.api.ServiceInterface
import com.theagent.inherited_classes.BaseFragment
import com.theagent.models.BaseModel
import com.theagent.models.Data
import com.theagent.models.LoginBase
import com.theagent.models.NewsModel
import com.theagent.util.SharedPref
import kotlinx.android.synthetic.main.home_fragment_layout.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class HomeFragment: BaseFragment() {

    private lateinit var player_name:MaterialTextView
    private lateinit var recycler_news:RecyclerView
    lateinit var viewAdapter: RecyclerNewsAdapter
    lateinit var matchAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    var list_Match:MutableList<Data> = ArrayList()
    private lateinit var apiCall:ServiceInterface
    private lateinit var apiCallHeader:ServiceInterface


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.home_fragment_layout,container,false)
        super.init(view)
        initView(view)
//        GlobalScope.launch (Dispatchers.Main) {   fetchmatch() }
        getNews()
        return  view
    }

    fun initView(view:View){
        apiCall = AgentServiceGenerator.create().create(ServiceInterface::class.java)
        apiCallHeader = AgentServiceGenerator.retrofitWithHeader().create(ServiceInterface::class.java)

        player_name = view.findViewById(R.id.tv_player_name)
        recycler_news = view.findViewById(R.id.recycler_news)

        player_name.setText(SharedPref.getSharedPreferences(context!!,SharedPref.NAME))
    }



    private fun goNewsDetails(data:NewsModel){
        var bundle:Bundle = Bundle()
        bundle.putParcelable("data",data)
        val navController: NavController = Navigation.findNavController(activity !!,R.id.nav_host_fragment)
        navController.navigate(R.id.action_homeFragment_to_newsDetailFragment,bundle)

    }

      fun getPlan(){
        val obj = JsonObject()
        obj.addProperty("language",SharedPref.getSharedPreferences(context!!,SharedPref.LANGUAGE))
        obj.addProperty("user_id",SharedPref.getSharedPreferences(context!!,SharedPref.USER_ID))
        apiCall.getPlan(obj).enqueue(object :Callback<LoginBase>{
            override fun onFailure(call: Call<LoginBase>, t: Throwable) {
               Log.e("TAG",t.message)
               hideIndicator()
            }

            override fun onResponse(call: Call<LoginBase>, response: Response<LoginBase>) {
                val  mainResp = response.body()

                if(mainResp?.status!!){
                    tv_plan.text = mainResp.basedata!!.name
                    tv_price.text ="SR " +mainResp.basedata.price+"/mo"
                    tv_expiray.text = mainResp.basedata.expiry_date
                }
            }

        })

     }


     fun fetchmatch() {
        val obj = JsonObject()
        obj.addProperty("language",SharedPref.getSharedPreferences(context!!,SharedPref.LANGUAGE))

        apiCallHeader.upcomingMatch(obj).enqueue(object :Callback<BaseModel>{
            override fun onFailure(call: Call<BaseModel>, t: Throwable) {
                Log.e("TAG",t.message)
                hideIndicator()
                getPlan()
            }

            override fun onResponse(call: Call<BaseModel>, response: Response<BaseModel>) {
                val result = response.body()

               getPlan()
                if(result != null){
                    list_Match = result!!.data as MutableList<Data>
                    viewManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
                    matchAdapter = RecyclerMatcheAdapter(context !!, list_Match) { id, pos ->
                        joinMatch(id,pos)
                    }
                    recycler_match.apply {
                        setHasFixedSize(true)
                        layoutManager = viewManager
                        adapter = matchAdapter
                    }
                }
            }
        })
    }

     fun getNews(){
        showIndicator(getString(R.string.plswait))
        val obj = JsonObject()
        obj.addProperty("language",SharedPref.getSharedPreferences(context!!,SharedPref.LANGUAGE))
        apiCall.getNews(obj).enqueue(object: Callback<JsonObject>{
            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                Log.e("TAG",t.printStackTrace().toString())
                hideIndicator()
                fetchmatch()
            }

            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                hideIndicator()
                val mainResp = response.body()
                fetchmatch()
                mainResp!!.get("status")
                val data = mainResp.get("data")
                val gson = GsonBuilder().create()
                var testModel1:List<NewsModel>  = gson.fromJson(data, Array<NewsModel>::class.java).toList()
                Log.e("NEWS",testModel1.size.toString())
                viewManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
                viewAdapter = RecyclerNewsAdapter(context !!, testModel1) { data-> goNewsDetails(data)}
                recycler_news.apply {
                    setHasFixedSize(true)
                    layoutManager = viewManager
                    adapter = viewAdapter
                }
            }
        })
    }




fun joinMatch(id:Int,position:Int){
        val obj = JsonObject()
        obj.addProperty("match_id",id)
        apiCallHeader.joinMatch(obj).enqueue(object :Callback<BaseModel>{
            override fun onFailure(call: Call<BaseModel>, t: Throwable) {
                Log.e("TAG",t.message)
            }

            override fun onResponse(call: Call<BaseModel>, response: Response<BaseModel>) {
                Log.e("TAG",response.message())

                val mainResp = response.body()
                Toast.makeText(context,mainResp!!.message, Toast.LENGTH_SHORT).show()
                if(mainResp?.status!!){
                    list_Match[position].is_join_match="1"
                    matchAdapter.notifyItemChanged(position)

                }

            }

        })
    }

    override fun onPause() {
        super.onPause()
        Log.e("TAG","player")

//        if(viewAdapter !=null){
//            viewAdapter.stopPlayer()
//        }

    }

}