package com.theagent.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.google.gson.JsonObject
import com.theagent.R
import com.theagent.api.AgentServiceGenerator
import com.theagent.api.ServiceInterface
import com.theagent.inherited_classes.BaseFragment
import com.theagent.models.Data
import com.theagent.util.SharedPref
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class PlayerDetailFragment:BaseFragment() {

    private lateinit var imag_player:ImageView
    private lateinit var player_name:TextView
    private lateinit var player_position:TextView
    private lateinit var img_chat:ImageView
    private lateinit var data:Data

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.playerdetail_fragment_layout,container,false)

        super.init(view)
        initView(view)
        clickListner()
        return view
    }

    fun initView(view: View){
        data = arguments!!.getParcelable<Data>("data")!!

        imag_player = view.findViewById(R.id.img_player)
        player_name = view.findViewById(R.id.tv_name)
        player_position = view.findViewById(R.id.tv_position)
        img_chat = view.findViewById(R.id.img_chat)

        Glide.with(context!!).load(data!!.profile_picture).into(imag_player)

        player_name.setText(data!!.name + " "+data.last_name)
        player_position.setText(data.position)

        Log.e("TAG",data!!.name)
    }

    fun clickListner(){

        img_chat.setOnClickListener { v: View? ->
            GlobalScope.launch (Dispatchers.Main) {   requestChat() }

        }
    }

    suspend fun requestChat(){
        showIndicator(getString(R.string.plswait))
        var apiCallHeader = AgentServiceGenerator.retrofitWithHeader().create(
            ServiceInterface::class.java)
        val obj = JsonObject()
        obj.addProperty("from", SharedPref.getSharedPreferences(context!!, SharedPref.LANGUAGE))
        obj.addProperty("to",data.id)
        var response =  apiCallHeader.chatRequest(obj)
        hideIndicator()
        if(response.get("status").asBoolean){
            var bundle:Bundle = Bundle()
            bundle.putParcelable("data",data)
            val navController: NavController = Navigation.findNavController(activity !!,R.id.nav_agent_fragment)
            navController.navigate(R.id.action_playerDetailFragment_to_chatFragmentAgent,bundle)
        }else{

        }

    }
}