package com.theagent.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.mancj.materialsearchbar.MaterialSearchBar
import com.theagent.R
import com.theagent.adapter.RecyclerSearchAdapter
import com.theagent.api.AgentServiceGenerator
import com.theagent.api.ServiceInterface
import com.theagent.inherited_classes.BaseFragment
import com.theagent.models.Data
import com.theagent.util.SharedPref
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class AgentHomeFragment:BaseFragment() {

    private lateinit var searchBar:MaterialSearchBar
    private lateinit var apiCall:ServiceInterface
    private lateinit var recycler_player:RecyclerView
    private lateinit var viewAdapter:RecyclerSearchAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.agent_homelayout,container,false)

        init(view)
        initView(view)
        clickListner()
        return view
    }

    private fun initView(view: View){
        apiCall = AgentServiceGenerator.retrofitWithHeader().create(ServiceInterface::class.java)

        searchBar = view.findViewById(R.id.searchBar)
        recycler_player = view.findViewById(R.id.recycler_player)


    }

    private fun clickListner(){
        searchBar.setOnSearchActionListener(object : MaterialSearchBar.OnSearchActionListener{
            override fun onButtonClicked(buttonCode: Int) {
                Log.e("TAG","Search")
            }

            override fun onSearchStateChanged(enabled: Boolean) {
               Log.e("TAG","STATE")
            }

            override fun onSearchConfirmed(text: CharSequence?) {
                Log.e("TAG",text.toString())
                searchBar.hideKeyboard()
                GlobalScope.launch (Dispatchers.Main) {   searchPlayer(text.toString()) }
            }

        })
    }

    suspend fun searchPlayer(name:String){
        showIndicator(getString(R.string.plswait))
        val obj = JsonObject()
        obj.addProperty("language", SharedPref.getSharedPreferences(context!!, SharedPref.LANGUAGE))
        obj.addProperty("name",name)
        val response = apiCall.searchPlayer(obj)
        hideIndicator()
        if(response.status!!){
            val viewManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL,false)
            viewAdapter = RecyclerSearchAdapter(context !!, response.data!!){
                data -> goPlayerDetail(data)
            }
            recycler_player.apply {
                setHasFixedSize(true)
                layoutManager = viewManager
                adapter = viewAdapter
            }
        }
        Log.e("TAG", response.data!!.size.toString())
    }

    private fun goPlayerDetail(data:Data){
        var bundle:Bundle = Bundle()
        bundle.putParcelable("data",data)
        val navController: NavController = Navigation.findNavController(activity !!,R.id.nav_agent_fragment)
        navController.navigate(R.id.action_homeFragmentAgent_to_playerDetailFragment,bundle)


    }

    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

}