package com.theagent.fragment

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.fasterxml.jackson.databind.JsonNode
import com.google.gson.JsonObject
import com.pubnub.api.PNConfiguration
import com.pubnub.api.PubNub
import com.pubnub.api.callbacks.PNCallback
import com.pubnub.api.callbacks.SubscribeCallback
import com.pubnub.api.enums.PNStatusCategory
import com.pubnub.api.models.consumer.PNPublishResult
import com.pubnub.api.models.consumer.PNStatus
import com.pubnub.api.models.consumer.history.PNHistoryResult
import com.pubnub.api.models.consumer.pubsub.PNMessageResult
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult
import com.theagent.R
import com.theagent.adapter.RecyclerChatAdapter
import com.theagent.api.AgentServiceGenerator
import com.theagent.api.ServiceInterface
import com.theagent.inherited_classes.BaseFragment
import com.theagent.models.ChatModel
import com.theagent.models.Data
import com.theagent.util.SharedPref
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap


class ChatFragment :BaseFragment(){

    private lateinit var pubnub:PubNub
    private lateinit var etMessage:EditText
    private lateinit var imgSend:ImageView
    private lateinit var recyclerChat:RecyclerView
    private lateinit var messagesAdapter: RecyclerChatAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager
    var list_chat:MutableList<ChatModel> = ArrayList()
    var isUser:Boolean = false
    var channel:String? = null
    var dateFormat: DateFormat = SimpleDateFormat("hh:mm a")
    var currentTime = dateFormat.format(Date()).toString()
    private lateinit var data:Data
    private lateinit var tv_name:TextView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.chat_layout,container,false)

        super.init(view)
        initView(view)
        clickListner()
        return view
    }

    private fun initView(view: View){
        data = arguments!!.getParcelable<Data>("data")!!
        channel = "the_agent"+SharedPref.getSharedPreferences(context!!,SharedPref.USER_ID)
        Log.e("TAG",data.to + " "+data.from)
        val pnConfiguration = PNConfiguration()

        pnConfiguration.setSubscribeKey("sub-c-a0194cd4-7636-11ea-808e-bad180999bc3")
        pnConfiguration.setPublishKey("pub-c-363879e5-8a6a-4e9a-a1bf-727d78e15f56")
        pnConfiguration.setUuid(SharedPref.getSharedPreferences(context!!,SharedPref.USER_ID))
        pnConfiguration.setSecure(true)


        pubnub = PubNub(pnConfiguration)

        etMessage = view.findViewById(R.id.et_message)
        imgSend = view.findViewById(R.id.img_send)
        recyclerChat = view.findViewById(R.id.recycler_chat)
        tv_name = view.findViewById(R.id.tv_name)
        var img_user = view.findViewById<CircleImageView>(R.id.img_user)

        tv_name.setText(data.name)
        Glide.with(context!!).load(data.profile_picture).into(img_user)
//        Glide.with(context!!).load(data.profile_picture).listener(object :RequestListener<Drawable>{
//            override fun onLoadFailed(
//                e: GlideException?,
//                model: Any?,
//                target: Target<Drawable>?,
//                isFirstResource: Boolean
//            ): Boolean {
//                return false
//            }
//
//            override fun onResourceReady(
//                resource: Drawable?,
//                model: Any?,
//                target: Target<Drawable>?,
//                dataSource: DataSource?,
//                isFirstResource: Boolean
//            ): Boolean {
//              img_user.setImageDrawable(resource)
//                return false
//            }
//
//        })
        setRecycler()
    }

    suspend fun getChatHistory(){
        showIndicator(getString(R.string.plswait))
        var apiCallHeader = AgentServiceGenerator.retrofitWithHeader().create(
            ServiceInterface::class.java)
        val obj = JsonObject()
        obj.addProperty("from", SharedPref.getSharedPreferences(context!!, SharedPref.USER_ID))
        obj.addProperty("to",data.user_id)
        obj.addProperty("chat_id",data.chat_id)
        var response = apiCallHeader.chatHistory(obj)
        if(response.status!!){
            for (i in response.data!!.size-1 downTo 0) {
                var msg = response.data!![i]
                var chat: ChatModel? = null
                if(msg.from ==SharedPref.getSharedPreferences(context!!, SharedPref.USER_ID)){
                    chat =  ChatModel(0,
                        msg.message!!,SharedPref.getSharedPreferences(context!!, SharedPref.USER_IMAGE),currentTime)
                    messagesAdapter.appendMessage(chat)
                }else{
                    chat = ChatModel(1,
                        msg.message!!, data.profile_picture!!,currentTime)

                    messagesAdapter.appendMessage(chat)
                }
            }
            for (msg in response.data!!){

            }
            recyclerChat.scrollToPosition( 0)
        }
    }

    private fun clickListner(){

        imgSend.setOnClickListener { v: View? ->

            val messageBuilder = HashMap<String,String>()
            messageBuilder.put("sender",SharedPref.getSharedPreferences(context!!,SharedPref.USER_ID))
            messageBuilder.put("reciver", data.user_id!!)
            messageBuilder.put("content",etMessage.text.toString())

            pubnub.publish()
                .message(messageBuilder)
                .channel("the_agent"+data.user_id)
                .async(object : PNCallback<PNPublishResult?>() {
                    override fun onResponse(result: PNPublishResult?, status: PNStatus) {
                        var text = etMessage.text.toString()
                        GlobalScope.launch (Dispatchers.Main) {  saveChat(text) }
                        var chat: ChatModel? = null
                        chat =
                            if(!SharedPref.getSharedPreferences(context!!, SharedPref.USER_IMAGE).isNullOrEmpty()){
                                ChatModel(0,etMessage.text.toString(),SharedPref.getSharedPreferences(context!!, SharedPref.USER_IMAGE),currentTime)
                            }else{
                                ChatModel(0,etMessage.text.toString(),"",currentTime)
                            }
                        messagesAdapter.appendlast(chat)
                        isUser = true


                        etMessage.text.clear()
                    }
                })
        }

        pubnub.subscribe().channels(Arrays.asList(channel)).execute()
        val messageJsonObject = JsonObject()

        messageJsonObject.addProperty("sender",SharedPref.getSharedPreferences(context!!,SharedPref.USER_ID))
        messageJsonObject.addProperty("reciver","82")
        messageJsonObject.addProperty("content","etMessage.text.toString()")
        pubnub.addListener(object : SubscribeCallback() {
            override fun status(pubnub: PubNub, status: PNStatus) {
                Log.e("TAGW",status.category.name)
                if (status.category == PNStatusCategory.PNUnexpectedDisconnectCategory) {
                    // This event happens when radio / connectivity is lost
                } else if (status.category == PNStatusCategory.PNConnectedCategory) {

                    // Connect event. You can do stuff like publish, and know you'll get it.
                    // Or just use the connected event to confirm you are subscribed for
                    // UI / internal notifications, etc
                    if (status.category == PNStatusCategory.PNConnectedCategory) {
                        pubnub.publish().channel(channel).message(messageJsonObject)
                            .async(object : PNCallback<PNPublishResult?>() {
                                override fun onResponse(
                                    result: PNPublishResult?,
                                    status: PNStatus
                                ) {
                                    // Check whether request successfully completed or not.
                                    if (!status.isError) {

                                        // Message successfully published to specified channel.
                                    } else {

                                    }
                                }
                            })
                    }
                } else if (status.category == PNStatusCategory.PNReconnectedCategory) {


                } else if (status.category == PNStatusCategory.PNDecryptionErrorCategory) {


                }
            }

            override fun message(pubnub: PubNub, message: PNMessageResult) {
                // Handle new message stored in message.message

                val jsonMsg: JsonNode = message.message

                val chat = ChatModel(1, jsonMsg.get("content").asText(),
                    data.profile_picture!!,currentTime)
                messagesAdapter.appendlast(chat)

                // extract desired parts of the payload, using Gson


            }

            override fun presence(pubnub: PubNub, presence: PNPresenceEventResult) {}
        })


        pubnub.history()
            .channel(channel) // where to fetch history from
            .count(100) // how many items to fetch
            .async(object : PNCallback<PNHistoryResult?>() {
                override fun onResponse(result: PNHistoryResult?, status: PNStatus) {
                    Log.e("TAGEE",result.toString())
                }
            })
    }

    fun setRecycler(){
        viewManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL,true)

        messagesAdapter = RecyclerChatAdapter(context !!, list_chat)
        recyclerChat.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = messagesAdapter
        }
        GlobalScope.launch (Dispatchers.Main) {  getChatHistory()}
    }

    suspend fun saveChat(message:String){
        val obj = JsonObject()
        obj.addProperty("from",SharedPref.getSharedPreferences(context!!,SharedPref.USER_ID))
        obj.addProperty("to",data.user_id)
        obj.addProperty("chat_id",data.chat_id)
        obj.addProperty("message",message)
        var apiCallHeader = AgentServiceGenerator.retrofitWithHeader().create(ServiceInterface::class.java)
        var response = apiCallHeader.saveChat(obj)
        Log.e("TAG",response.get("message").asString)
    }
}