package com.theagent.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.google.gson.GsonBuilder
import com.theagent.R
import com.theagent.adapter.RecyclerChatRequestAdapter
import com.theagent.adapter.RecyclerNewsAdapter
import com.theagent.api.AgentServiceGenerator
import com.theagent.api.ServiceInterface
import com.theagent.inherited_classes.BaseFragment
import com.theagent.models.Data
import com.theagent.util.SharedPref
import kotlinx.android.synthetic.main.home_fragment_layout.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class InboxFragment:BaseFragment() {

    private lateinit var recycler_chatrequest:RecyclerView
    private lateinit var ll_inbox:LinearLayout

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.inbox_layout,container,false)

        super.init(view)
        initView(view)
        GlobalScope.launch (Dispatchers.Main) {   getChatRequest() }
        return view
    }

    private fun initView(view: View){
        recycler_chatrequest = view.findViewById(R.id.recycler_chatrequest)
        ll_inbox = view.findViewById(R.id.ll_inbox)
    }

    suspend fun getChatRequest(){
        showIndicator(getString(R.string.plswait))
        var apiCallHeader = AgentServiceGenerator.retrofitWithHeader().create(
            ServiceInterface::class.java)
        var response = apiCallHeader.getChatRequests()
        hideIndicator()
        Snackbar.make(ll_inbox, response.get("message").asString, Snackbar.LENGTH_SHORT)
            .show()
        if(response.get("status").asBoolean){
            val gson = GsonBuilder().create()
            var data:List<Data>  = gson.fromJson( response.get("data"), Array<Data>::class.java).toList()
           var viewManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL,false)
           var  viewAdapter = RecyclerChatRequestAdapter(context !!, data) { data-> goToChat(data)}
            recycler_chatrequest.apply {
                setHasFixedSize(true)
                layoutManager = viewManager
                adapter = viewAdapter
            }
        }
    }

    fun goToChat(data: Data){
        var bundle:Bundle = Bundle()
        bundle.putParcelable("data",data)
        if(SharedPref.getSharedPreferences(context!!,SharedPref.USER_TYPE)=="player"){
            val navController: NavController = Navigation.findNavController(activity !!,R.id.nav_host_fragment)
            navController.navigate(R.id.action_inboxFragment_to_chatFragment,bundle)

        }else{
            val navController: NavController = Navigation.findNavController(activity !!,R.id.nav_agent_fragment)
            navController.navigate(R.id.action_inboxFragment2_to_chatFragmentAgent,bundle)
        }

    }
}