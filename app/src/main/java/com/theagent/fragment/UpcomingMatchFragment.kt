package com.theagent.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.theagent.R
import com.theagent.adapter.RecyclerMatcheAdapter
import com.theagent.api.AgentServiceGenerator
import com.theagent.api.ServiceInterface
import com.theagent.inherited_classes.BaseFragment
import com.theagent.models.BaseModel
import com.theagent.models.Data
import com.theagent.util.SharedPref
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class UpcomingMatchFragment:BaseFragment() {

    private lateinit var recycler_stats: RecyclerView
    var list_Match:MutableList<Data> = ArrayList()
    lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.stats_layout,container,false)

        super.init(view)
        initView(view)
        GlobalScope.launch (Dispatchers.Main) {   getPlayerStats() }

        return view
    }

    private fun initView(view:View){
        recycler_stats = view.findViewById(R.id.recycler_stats)

    }

    private fun clickListner(){

    }

    suspend fun getPlayerStats(){
        showIndicator(context!!.getString(R.string.plswait))
        val obj = JsonObject()
        obj.addProperty("language", SharedPref.getSharedPreferences(context!!, SharedPref.LANGUAGE))
        val apiCall = AgentServiceGenerator.retrofitWithHeader().create(ServiceInterface::class.java)
        val result = apiCall.upcomingmatch(obj)
        hideIndicator()
        list_Match = result!!.data as MutableList<Data>
                viewManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL,false)
                viewAdapter = RecyclerMatcheAdapter(context !!, list_Match) { id, pos ->
                    joinMatch(id,pos)
                }
                recycler_stats.apply {
                    setHasFixedSize(true)
                    layoutManager = viewManager
                    adapter = viewAdapter
                }

//        apiCall.upcomingmatch(obj).enqueue(object: Callback<BaseModel> {
//            override fun onFailure(call: Call<BaseModel>, t: Throwable) {
//                hideIndicator()
//                Log.e("TAG",t.printStackTrace().toString())
//            }
//
//            override fun onResponse(call: Call<BaseModel>, response: Response<BaseModel>) {
//
//
//        })
    }

    private fun joinMatch(id:Int,position:Int){
        val obj = JsonObject()
        obj.addProperty("match_id",id)
        val apiCall = AgentServiceGenerator.retrofitWithHeader().create(ServiceInterface::class.java)
        apiCall.joinMatch(obj).enqueue(object :Callback<BaseModel>{
            override fun onFailure(call: Call<BaseModel>, t: Throwable) {
                Log.e("TAG",t.message)
            }

            override fun onResponse(call: Call<BaseModel>, response: Response<BaseModel>) {
                Log.e("TAG",response.message())

                val mainResp = response.body()
                Toast.makeText(context,mainResp!!.message, Toast.LENGTH_SHORT).show()
                if(mainResp?.status!!){
                    list_Match[position].is_join_match="1"
                    viewAdapter.notifyItemChanged(position)

                }

            }

        })
    }
}