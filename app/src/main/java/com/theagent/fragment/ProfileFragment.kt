package com.theagent.fragment

import android.app.DatePickerDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.button.MaterialButton
import com.google.android.material.snackbar.Snackbar
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.theagent.BuildConfig
import com.theagent.R
import com.theagent.activity.AgentActivity
import com.theagent.activity.HomeActivity
import com.theagent.adapter.RecyclerImageAdapter
import com.theagent.adapter.RecyclerVideoAdapter
import com.theagent.api.AgentServiceGenerator
import com.theagent.api.ServiceInterface
import com.theagent.inherited_classes.BaseFragment
import com.theagent.models.BaseModel
import com.theagent.models.Video
import com.theagent.util.SharedPref
import kotlinx.android.synthetic.main.profile_layout.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class ProfileFragment:BaseFragment(), HomeActivity.BottomClick, AgentActivity.BottomClick {


    private lateinit var et_name:EditText
    private lateinit var et_last:EditText
    private lateinit var et_email:EditText
    private lateinit var et_address:EditText
    private lateinit var et_number:EditText
    private lateinit var et_date:EditText
    private lateinit var et_height:EditText
    private lateinit var et_weight:EditText
    private lateinit var et_blood:EditText
    private lateinit var tv_addVIdeo:TextView
    private lateinit var recycler_video:RecyclerView
    private lateinit var recycler_image:RecyclerView
    private lateinit var user_img:ImageView
    private lateinit var btn_save:MaterialButton
    private lateinit var ll_profile:LinearLayout
    lateinit var viewPhotoAdapter: RecyclerView.Adapter<*>
    lateinit var viewVideoAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    internal lateinit var bottomFragment:RoundedBottomSheetDialogFragment

    var list_video:MutableList<Video> = ArrayList()
    var list_picture:MutableList<Video> = ArrayList()
    var isVideoClick:Boolean = false

    val myCalendar: Calendar = Calendar.getInstance()
    lateinit var currentPhotoPath: String


    private val FINAL_TAKE_PHOTO = 1
    private val FINAL_CHOOSE_PHOTO = 2
    private lateinit var apiCall:ServiceInterface

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.profile_layout,container,false)

        super.init(view)
        initView(view)
        getPriofile()
        clickListner()

        if (SharedPref.getSharedPreferences(context!!,SharedPref.USER_TYPE) == "player"){
            (context as HomeActivity).initBottomInterface(this@ProfileFragment)
        }else{
            (context as AgentActivity).initBottomInterface(this@ProfileFragment)
        }


        return view
    }

    private fun initView(view:View){
        apiCall = AgentServiceGenerator.retrofitWithHeader().create(ServiceInterface::class.java)

        et_address = view.findViewById(R.id.et_address)
        et_name= view.findViewById(R.id.et_name)
        et_last = view.findViewById(R.id.et_last)
        et_email = view.findViewById(R.id.et_mail)
        et_number = view.findViewById(R.id.et_number)
        et_date = view.findViewById(R.id.et_dob)
        et_height = view.findViewById(R.id.et_height)
        et_weight = view.findViewById(R.id.et_weight)
        et_blood = view.findViewById(R.id.et_blood)
        recycler_video = view.findViewById(R.id.recycler_video)
        recycler_image = view.findViewById(R.id.recycler_image)
        tv_addVIdeo = view.findViewById(R.id.tv_addVIdeo)
        user_img = view.findViewById(R.id.user_img)
        btn_save = view.findViewById(R.id.btn_save)
        ll_profile = view.findViewById(R.id.ll_profile)


    }

    private fun clickListner(){

        tv_addVIdeo.setOnClickListener { v: View? ->
            bottomFragment = RoundedBottomSheetDialogFragment()
            val args = Bundle()
            args.putBoolean("isVideo", true)
            bottomFragment.arguments = args
            bottomFragment.show(activity!!.supportFragmentManager, bottomFragment.tag)
        }

        user_img.setOnClickListener { v: View? ->
            bottomFragment = RoundedBottomSheetDialogFragment()
            val args = Bundle()
            args.putBoolean("isVideo", false)
            bottomFragment.arguments = args
            bottomFragment.show(activity!!.supportFragmentManager, bottomFragment.tag)
        }

        et_date.setOnClickListener { v: View? ->
            showCalendar()
        }

        btn_save.setOnClickListener { v: View? ->
            updateProfile()
        }

    }

    private fun getPriofile(){
        showIndicator("Fetching Profile")
      apiCall.getProfile().enqueue(object :Callback<JsonObject>{
          override fun onFailure(call: Call<JsonObject>, t: Throwable) {
              Log.e("TAG",t.message)
              hideIndicator()
          }

          override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
              val mainResp = response.body()
              hideIndicator()
              if(mainResp != null){
                  if(mainResp!!.get("status").asBoolean){

                      val data = mainResp!!.getAsJsonObject("data")
                      et_address.setText(data.get("address").asString)
                      et_name.setText(data.get("first_name").asString)
                      et_last.setText(data.get("last_name").asString)
                      et_number.setText(data.get("phone_number").asString)
                      et_blood.setText(data.get("blood_group").asString)
                      et_date.setText(data.get("date_of_birth").asString)
                      et_height.setText(data.get("height").asString)
                      et_weight.setText(data.get("weight").asString)
                      et_mail.setText(data.get("email").asString)
                      et_nation.setText(data.get("nationality").asString)

                      val gson = GsonBuilder().create()
                      val video = data.get("video")
                      list_video = gson.fromJson(video, Array<Video>::class.java).toList().toMutableList()
                      if(list_video!!.size>0){
                          setVideoRecycler()
                      }
                      val photo = data.get("profile_picture")
                      list_picture = gson.fromJson(photo, Array<Video>::class.java).toList().toMutableList()
                      if(list_picture.size>0){
                          setPictureRecycler()
                      }

                  }
              }
              }



      })
    }


    private fun setVideoRecycler(){
        recycler_video.visibility = View.VISIBLE
        viewManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false)
        viewVideoAdapter = RecyclerVideoAdapter(context !!, list_video){id,pos->
            GlobalScope.launch (Dispatchers.Main) {  removeImage(id,pos,false)   }
        }
        recycler_video.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewVideoAdapter
        }
    }

    private fun setPictureRecycler(){
        Glide.with(context!!).load(list_picture!!.get(0).url).into(user_img)
        SharedPref.setSharedPreferences(context!!,list_picture!!.get(0).url,SharedPref.USER_IMAGE)
        viewManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false)
        viewPhotoAdapter = RecyclerImageAdapter(context !!, list_picture){id,pos->
            GlobalScope.launch (Dispatchers.Main) {  removeImage(id,pos,true)   }

        }
        recycler_image.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewPhotoAdapter
        }
    }

    suspend fun removeImage(id:String,pos:Int,isImage:Boolean){
        if(id == "-1"){
            if(isImage){
                list_picture.removeAt(pos)
                viewPhotoAdapter.notifyItemRemoved(pos)
            }else{
                list_video.removeAt(pos)
                viewVideoAdapter.notifyItemRemoved(pos)
            }
        }else{
            showIndicator(getString(R.string.plswait))
            var response = apiCall.removeMedia(id)
            hideIndicator()
            if(response.get("status").asBoolean){
                if(isImage){
                    list_picture.removeAt(pos)
                    viewPhotoAdapter.notifyItemRemoved(pos)
                }else{
                    list_video.removeAt(pos)
                    viewVideoAdapter.notifyItemRemoved(pos)
                }

            }
        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == FINAL_CHOOSE_PHOTO && resultCode == -1){
            var temp = RealPathUtil.getRealPathFromURIAPI19(context!!, data!!.data!!)
            Log.e("TAG",temp)
            var video = Video(-1,temp.toString(),1)
            if(isVideoClick){
                list_video.add(video)
                if(list_video.size > 1){
                    viewVideoAdapter.notifyDataSetChanged()
                }else{
                    setVideoRecycler()
                }

            }else{

                list_picture.add(video)
                if(list_picture.size>1){
                    viewPhotoAdapter.notifyDataSetChanged()
                }else{
                    setPictureRecycler()
                }

            }
        }else if(requestCode == FINAL_TAKE_PHOTO && resultCode ==-1 ){
            var video = Video(-1,currentPhotoPath,1)
            if(isVideoClick){
                list_video.add(video)
                if(list_video.size>1){
                    viewVideoAdapter.notifyDataSetChanged()
                }else{
                    setVideoRecycler()
                }



            }else{

                list_picture.add(video)
                if(list_picture.size>1){
                    viewPhotoAdapter.notifyDataSetChanged()
                }else{
                    setPictureRecycler()
                }

            }
        }
    }

    override fun onCameraLick(isVideo: Boolean) {
        isVideoClick = isVideo
        bottomFragment.dismiss()
        if(isVideo){
            dispatchTakePictureIntent(MediaStore.ACTION_VIDEO_CAPTURE)
        }else{
            dispatchTakePictureIntent(MediaStore.ACTION_IMAGE_CAPTURE)
        }


    }

    override fun onGalleryClick(isVideo: Boolean) {
        isVideoClick = isVideo
        if(isVideo){
            bottomFragment.dismiss()
            val intent = Intent("android.intent.action.GET_CONTENT")
            PictureIntent("android.intent.action.GET_CONTENT")
            intent.type = "video//*"
            startActivityForResult(intent, FINAL_CHOOSE_PHOTO)

        }else{
            bottomFragment.dismiss()
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "image/*"
            PictureIntent("android.intent.action.GET_CONTENT")

            startActivityForResult(intent, FINAL_CHOOSE_PHOTO)


        }

    }

    private fun dispatchTakePictureIntent(type:String) {
        Intent(type).also { takePictureIntent ->
            takePictureIntent.resolveActivity(context!!.packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    // Error occurred while creating the File

                    null
                }
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        context!!,
                        BuildConfig.APPLICATION_ID + ".provider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, FINAL_TAKE_PHOTO)
                }
            }
        }
    }

    private fun PictureIntent(type:String) {
        Intent(type).also { takePictureIntent ->
            takePictureIntent.resolveActivity(context!!.packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    // Error occurred while creating the File

                    null
                }
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        context!!,
                        BuildConfig.APPLICATION_ID + ".provider",
                        it
                    )
                    takePictureIntent.type = "image/*"
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, FINAL_TAKE_PHOTO)
                }
            }
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File = context!!.getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = absolutePath
        }
    }

    fun showCalendar(){
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)


        val dpd = DatePickerDialog(context!!, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

            Log.e("DTE",dayOfMonth.toString()  +" "+monthOfYear + " "+year)
            val month = monthOfYear+1
            et_date.setText(year.toString()+"-"+month+"-"+dayOfMonth)

        }, year, month, day
        )

        dpd.show()
    }

   private fun updateProfile(){
        showIndicator("")
       val name: RequestBody =
           RequestBody.create(MediaType.parse("text/plain"), et_name.text.toString())
       val last_name: RequestBody =
           RequestBody.create(MediaType.parse("text/plain"), et_last.text.toString())
       val email: RequestBody =
           RequestBody.create(MediaType.parse("text/plain"), et_mail.text.toString())
       val nattionality: RequestBody =
           RequestBody.create(MediaType.parse("text/plain"), et_nation.text.toString())
       val address: RequestBody =
           RequestBody.create(MediaType.parse("text/plain"), et_address.text.toString())
       val number: RequestBody =
           RequestBody.create(MediaType.parse("text/plain"), et_number.text.toString())
       val dob: RequestBody =
           RequestBody.create(MediaType.parse("text/plain"), et_date.text.toString())
       val height: RequestBody =
           RequestBody.create(MediaType.parse("text/plain"), et_height.text.toString())
       val weight: RequestBody =
           RequestBody.create(MediaType.parse("text/plain"), et_weight.text.toString())
       val blood: RequestBody =
           RequestBody.create(MediaType.parse("text/plain"), et_blood.text.toString())
       val partsImage: MutableList<MultipartBody.Part> = ArrayList()
       val partsVideo: MutableList<MultipartBody.Part> = ArrayList()


       Log.e("TAG",list_picture.size.toString())
       for (pic in list_picture){
           if(pic.id == -1){
               val file = File(pic.url)
               val fbody: RequestBody = RequestBody.create(MediaType.parse("image/*"), file)
               partsImage.add(MultipartBody.Part.createFormData("image[]", file.name, fbody))
           }
       }
       for (video in list_video){
           if(video.id == -1){
               val file = File(video.url)
               val fbody: RequestBody = RequestBody.create(MediaType.parse("image/*"), file)
                 partsVideo.add(MultipartBody.Part.createFormData("video[]", file.name, fbody))
           }
       }
       var call: Call<BaseModel>? = null
       if(partsImage.size == 0 && partsVideo.size == 0){
           call = apiCall.updateWithoutFile(name,last_name,email,nattionality,address,number,dob,height,weight,blood)
       }else if(partsImage.size == 0 && partsVideo.size>0){
           call = apiCall.updateWithtFile(name,last_name,email,nattionality,address,number,dob,height,weight,blood,partsVideo)
       }else if(partsImage.size>0 && partsVideo.size == 0){
           call = apiCall.updateWithtFile(name,last_name,email,nattionality,address,number,dob,height,weight,blood,partsImage)
       }else{
           call = apiCall.updateProfile(name,last_name,email,nattionality,address,number,dob,height,weight,blood,
               partsImage,partsVideo)
       }
       call!!.enqueue(object :Callback<BaseModel>{
           override fun onFailure(call: Call<BaseModel>, t: Throwable) {
               hideIndicator()
               Log.e("ERRR",t.message)
           }

           override fun onResponse(call: Call<BaseModel>, response: Response<BaseModel>) {
              Log.e("TAG",response.message())
               hideIndicator()
               val mainResp = response.body()
               Snackbar.make(ll_profile, mainResp!!.message!!, Snackbar.LENGTH_SHORT)
                   .show()
               if(mainResp!!.status!!){

               }
           }

       })
   }

}