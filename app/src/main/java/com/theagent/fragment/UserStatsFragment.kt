package com.theagent.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.theagent.R
import com.theagent.adapter.RecyclerMatcheAdapter
import com.theagent.api.AgentServiceGenerator
import com.theagent.api.ServiceInterface
import com.theagent.inherited_classes.BaseFragment
import com.theagent.models.BaseModel
import com.theagent.models.Data
import com.theagent.util.SharedPref
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class UserStatsFragment:BaseFragment() {

    private lateinit var recycler_stats:RecyclerView
    var list_Match:MutableList<Data> = ArrayList()
    lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.stats_layout,container,false)

        super.init(view)
        initView(view)
        getPlayerStats()
        return view
    }

    private fun initView(view:View){
        recycler_stats = view.findViewById(R.id.recycler_stats)

    }

    private fun clickListner(){

    }

    private fun getPlayerStats(){
        showIndicator(context!!.getString(R.string.plswait))
        val obj = JsonObject()
        obj.addProperty("language",SharedPref.getSharedPreferences(context!!,SharedPref.LANGUAGE))
        val apiCall = AgentServiceGenerator.retrofitWithHeader().create(ServiceInterface::class.java)
        apiCall.getPlayerStats(obj).enqueue(object: Callback<BaseModel> {
            override fun onFailure(call: Call<BaseModel>, t: Throwable) {
                hideIndicator()
                Log.e("TAG",t.printStackTrace().toString())
            }

            override fun onResponse(call: Call<BaseModel>, response: Response<BaseModel>) {
                hideIndicator()
                val mainResp = response.body()
                list_Match = mainResp!!.data as MutableList<Data>
                viewManager = LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false)
                viewAdapter = RecyclerMatcheAdapter(context !!, list_Match) { id, pos ->
                    updateStats(id,pos)
                }
                recycler_stats.apply {
                    setHasFixedSize(true)
                    layoutManager = viewManager
                    adapter = viewAdapter
                }
            }

        })
    }

    private fun updateStats(id:Int,position:Int){
//
        var bundle:Bundle = Bundle()
        bundle.putString("team1", list_Match[position].team1)
        bundle.putString("team2", list_Match[position].team2)
        bundle.putString("short_name1", list_Match[position].short_name1)
        bundle.putString("short_name2",list_Match.get(position).short_name2)
        bundle.putString("date",list_Match.get(position).date)
        bundle.putString("time",list_Match.get(position).time)
        bundle.putString("match_id",id.toString())

        val navController: NavController = Navigation.findNavController(activity !!,R.id.nav_host_fragment)
        navController.navigate(R.id.action_userStatsFragment_to_updateStatsFragment,bundle)
    }
}