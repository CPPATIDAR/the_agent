package com.theagent.util

import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import java.util.*



class LocaleManager {


    val ENGLISH = "en"
    val HINDI = "hi"
    val SPANISH = "es"

    companion object{
        fun updateResources(context: Context, language: String?): Context? {
            var context: Context = context
            val locale = Locale(language)
            Locale.setDefault(locale)
            val res: Resources = context.getResources()
            val config = Configuration(res.getConfiguration())
            if (Build.VERSION.SDK_INT >= 17) {
                config.setLocale(locale)
                context = context.createConfigurationContext(config)
            } else {
                config.locale = locale
                res.updateConfiguration(config, res.getDisplayMetrics())
            }
            return context
        }

        fun getLocale(res: Resources): Locale? {
            val config: Configuration = res.getConfiguration()
            return if (Build.VERSION.SDK_INT >= 24) config.getLocales().get(0) else config.locale
        }
    }



}