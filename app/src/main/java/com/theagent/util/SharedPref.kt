package com.theagent.util

import android.content.Context

class SharedPref {

    companion object{

        val SP_NAME = "SP_AGENT"
        val TOKEN_DATA = "token_data"
        val AUTH_TYPE= "auth_type"
        val NAME = "first_name"
        val LAST_NAME ="last_name"
        val EMAIL = "email"
        val PROFILE_DATA = "profile_data"
        val PHONE_NUM = "phone_number"
        val USER_ID = "user_id"
        val LANGUAGE ="language"
        val USER_IMAGE ="user_image"
        val USER_TYPE = "user_type"


        fun setSharedPreferences(context: Context, value: String, key: String) {
            context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE).edit().putString(key, value)
                .commit()
        }

        fun getSharedPreferences(context: Context, key: String): String {
            return context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE).getString(key, "")!!
        }

        fun clearSharedPreferences(context: Context) {
            context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE).edit().clear().commit()
        }
    }


}