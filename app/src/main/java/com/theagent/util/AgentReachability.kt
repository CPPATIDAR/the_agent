package com.theagent.util

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import androidx.localbroadcastmanager.content.LocalBroadcastManager

class AgentReachability constructor(val mAppContext: Context) {

    private var mNetworkConnected: Boolean? = false

    companion object{
        var GPR_REACHABILITY_CHANGED_EVENT = "gpr_reachability_changed"
        var GPR_REACHABILITY_INTERNET_CONNECTED = "internet_connected"
        private var sReachability: AgentReachability? = null

        @Synchronized
        operator fun get(context: Context): AgentReachability {

            if (sReachability == null) {
                sReachability = AgentReachability(context.applicationContext)
            }

            return sReachability as AgentReachability
        }
    }

    fun startListeningForNetworkChanges(applicationContext: Context) {
        val connectivityManager =
            applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if (connectivityManager != null) {
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            mNetworkConnected =
                activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting

            val builder = NetworkRequest.Builder()
            connectivityManager.registerNetworkCallback(
                builder.build(),
                mNetworkChangeCallback
            )


        }
    }

    fun stopListeningForNetworkChanges(applicationContext: Context) {
        val connectivityManager =
            applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if (connectivityManager != null) {
            connectivityManager.unregisterNetworkCallback(mNetworkChangeCallback)


        }
    }

    fun networkConnected(): Boolean? {
        return mNetworkConnected
    }

    private fun onNetworkConnectivityChanged() {
        val connectivityManager =
            mAppContext?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {

            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            val networkConnected =
                activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting

            if (networkConnected !== mNetworkConnected) {
                mNetworkConnected = networkConnected
                val intent = Intent(GPR_REACHABILITY_CHANGED_EVENT)
                intent.putExtra(GPR_REACHABILITY_INTERNET_CONNECTED, mNetworkConnected!!)
                LocalBroadcastManager.getInstance(mAppContext!!).sendBroadcast(intent)


            }
        }
    }

    internal var mNetworkChangeCallback: ConnectivityManager.NetworkCallback = object :
        ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            onNetworkConnectivityChanged()
        }

        override fun onLost(network: Network) {
            onNetworkConnectivityChanged()
        }
    }
}