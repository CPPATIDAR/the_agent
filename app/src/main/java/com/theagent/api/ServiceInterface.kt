package com.theagent.api

import com.google.gson.JsonObject
import com.theagent.constant.Constants
import com.theagent.models.BaseModel
import com.theagent.models.LoginBase
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.http.*

interface ServiceInterface {


    @POST(Constants.GET_PLANS)
    fun getPlans(@Body postData:JsonObject):Call<BaseModel>

    @POST(Constants.GET_REGION)
    fun getRegion(@Body postData: JsonObject):Call<BaseModel>

    @POST(Constants.GET_COUNTRY)
    fun getCountry(@Body postData: JsonObject):Call<BaseModel>

    @POST(Constants.GET_STATE)
    fun getState(@Body postData: JsonObject):Call<BaseModel>
    @POST(Constants.GET_CITY)
    fun getCity(@Body postData: JsonObject):Call<BaseModel>

    @FormUrlEncoded
    @POST(Constants.SIGNUP)
    fun registerUser(@Field("name") name:String,@Field("email")email:String,@Field("password")
    password:String,@Field("nationality")nation:String,@Field("phone_number")number:String,
    @Field("address")address:String,@Field("password_confirmation")pswdConf:String,
    @Field("terms_condition")terms:String,@Field("num_matches")match:String,
    @Field("token")token:String,@Field("device_type")device:String,
    @Field("plan_id")plan_id:String,@Field("region_id")region_id:String,
     @Field("state_id")state_id:String,@Field("city_id")city_id:String):Call<LoginBase>

    @POST(Constants.LOGIN)
    fun loginUser(@Body postData: JsonObject):Call<LoginBase>

    @POST(Constants.NEWS)
    fun getNews(@Body postData: JsonObject):Call<JsonObject>

    @POST(Constants.CURRENT_PLAN)
     fun getPlan(@Body postData: JsonObject):Call<LoginBase>


    @POST(Constants.UPCOMING_MATCH)
    suspend fun upcomingmatch(@Body postData: JsonObject):BaseModel

    @POST(Constants.UPCOMING_MATCH)
    fun upcomingMatch(@Body postData: JsonObject):Call<BaseModel>

    @POST(Constants.JOIN_MATCH)
    fun joinMatch(@Body postData:JsonObject) :Call<BaseModel>

    @POST(Constants.GET_PROFILE)
    fun getProfile() :Call<JsonObject>

    @POST(Constants.PLAYER_STATS)
    fun getPlayerStats(@Body postData: JsonObject):Call<BaseModel>

    @POST(Constants.PLAYER_PERFOMMANCE)
    fun getStats(@Body postData: JsonObject):Call<LoginBase>

    @FormUrlEncoded
    @POST(Constants.UPDATE_STATS)
    fun updateSatus(@Field("match_id") match_id:String,@Field("goals") goals:String,
                    @FieldMap hashFields:HashMap<String, String> ):Call<BaseModel>

    @Multipart
    @POST(Constants.UPDATE_PROFILE)
    fun updateProfile(@Part("first_name") first_name: RequestBody, @Part("last_name")
                last_name:RequestBody, @Part("email")email:RequestBody, @Part("nationality")nation:RequestBody, @Part("phone_number")number:RequestBody,
                      @Part("address")address:RequestBody, @Part("date_of_birth")date_of_birth:RequestBody,
                      @Part("height")height:RequestBody, @Part("weight")weight:RequestBody,
                      @Part("blood_group")blood_group:RequestBody,@Part image: List<MultipartBody.Part>,@Part video:  List<MultipartBody.Part>):Call<BaseModel>
    @Multipart
    @POST(Constants.UPDATE_PROFILE)
    fun updateWithoutFile(@Part("first_name") first_name: RequestBody, @Part("last_name")
    last_name:RequestBody, @Part("email")email:RequestBody, @Part("nationality")nation:RequestBody, @Part("phone_number")number:RequestBody,
                      @Part("address")address:RequestBody, @Part("date_of_birth")date_of_birth:RequestBody,
                      @Part("height")height:RequestBody, @Part("weight")weight:RequestBody,
                      @Part("blood_group")blood_group:RequestBody):Call<BaseModel>
    @Multipart
    @POST(Constants.UPDATE_PROFILE)
    fun updateWithtFile(@Part("first_name") first_name: RequestBody, @Part("last_name")
    last_name:RequestBody, @Part("email")email:RequestBody, @Part("nationality")nation:RequestBody, @Part("phone_number")number:RequestBody,
                          @Part("address")address:RequestBody, @Part("date_of_birth")date_of_birth:RequestBody,
                          @Part("height")height:RequestBody, @Part("weight")weight:RequestBody,
                          @Part("blood_group")blood_group:RequestBody,@Part image: List<MultipartBody.Part>):Call<BaseModel>



    @POST(Constants.TRANSACTION_HISTORY)
    fun getTransaction(@Body postData: JsonObject):Call<LoginBase>

    @FormUrlEncoded
    @POST(Constants.GET_STATICS)
    suspend fun getStatics(@Field("language") lang:String):BaseModel

    @FormUrlEncoded
    @POST(Constants.REMOVE_MEDIA)
    suspend fun removeMedia(@Field("media_id") media_id:String):JsonObject

    @POST(Constants.SEARCH_PLAYER)
    suspend fun searchPlayer(@Body postData: JsonObject):BaseModel

    @POST(Constants.SAVE_CHAT)
    suspend fun saveChat(@Body postData: JsonObject):JsonObject

    @POST(Constants.GET_CHAT_HISTORY)
    suspend fun chatHistory(@Body postData: JsonObject):BaseModel

    @POST(Constants.GET_FAV)
    suspend fun getFav(@Body postData: JsonObject):JsonObject

    @POST(Constants.IS_FAV)
    suspend fun setFav(@Body postData: JsonObject):JsonObject

    @POST(Constants.CHAT_REQUEST)
    suspend fun chatRequest(@Body postData: JsonObject):JsonObject

    @POST(Constants.GET_CHAT_REQUEST)
    suspend fun getChatRequests():JsonObject



}