package com.theagent.api

import com.theagent.BuildConfig
import com.theagent.constant.Constants
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class AgentServiceGenerator {

    companion object{

        fun create() : Retrofit {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BuildConfig.BASE_URL)
                .build()

            return retrofit
        }

        fun retrofitWithHeader() :Retrofit{

             val httpClient = OkHttpClient.Builder().connectTimeout(300, TimeUnit.SECONDS).
                 readTimeout(100,TimeUnit.SECONDS)

                httpClient.addInterceptor(object : Interceptor {
                    override fun intercept(chain: Interceptor.Chain): Response {
                        val req = chain.request()

                        val mainReq = req.newBuilder()
                            .addHeader("Content-Type","application/json")
                            .addHeader("Authorization", Constants.getHeader())
                            .method(req.method(),req.body())
                            .build()
                        return chain.proceed(mainReq)
                    }

                })
             val main_HttpClient = httpClient.build()

             val retrofit = Retrofit.Builder()
                 .addConverterFactory(GsonConverterFactory.create())
                 .baseUrl(BuildConfig.BASE_URL)
                 .client(main_HttpClient)
                 .build()

             return retrofit
         }

    }
}