package com.theagent.inherited_classes

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.github.ybq.android.spinkit.SpinKitView
import com.theagent.R
import com.theagent.util.AgentReachability

open class BaseActivity : AppCompatActivity() {


    var tv_basic: TextView?=null
    var loadingIndicatior:SpinKitView?=null
    var activityIndicator: FrameLayout?=null
    var interntConnected: LinearLayout?=null


    fun init(){

        tv_basic = findViewById(R.id.tv_basic)
        loadingIndicatior= findViewById(R.id.avIndicator)
        activityIndicator= findViewById(R.id.activityIndicator)
        interntConnected = findViewById(R.id.ll_interntConnected)

        AgentReachability.get(applicationContext).startListeningForNetworkChanges(applicationContext)
        if(AgentReachability.get(applicationContext).networkConnected()!!){
            hideInternetWarning()
        }else{
            showInternetWarning()
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
            IntentFilter(AgentReachability.GPR_REACHABILITY_CHANGED_EVENT)
        )
    }

    fun showInternetWarning(){
        interntConnected!!.visibility = View.VISIBLE
    }

    fun hideInternetWarning(){
        interntConnected!!.visibility = View.GONE
    }

    fun hideIndicator(){
        activityIndicator!!.visibility = View.GONE
    }

    fun showIndicator(text:String){
        activityIndicator!!.visibility = View.VISIBLE

        tv_basic!!.setText(text)
    }

    @SuppressLint("ServiceCast")
    fun isNetworkConnected():Boolean{
        val connectivityManager=getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo=connectivityManager.activeNetworkInfo
        return  networkInfo!=null && networkInfo.isConnected
    }

    private val mMessageReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            // Get extra data included in the Intent
            val networkConnected =
                intent.getBooleanExtra(AgentReachability.GPR_REACHABILITY_INTERNET_CONNECTED, false)
            if (networkConnected) {
                hideInternetWarning()
            } else {
                showInternetWarning()
            }
        }
    }

}