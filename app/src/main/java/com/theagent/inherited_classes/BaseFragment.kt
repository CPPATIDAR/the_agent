package com.theagent.inherited_classes

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.github.ybq.android.spinkit.SpinKitView
import com.theagent.R
import com.theagent.util.AgentReachability

open class BaseFragment:Fragment() {

    var tv_basic: TextView?=null
    var loadingIndicatior: SpinKitView?=null
    var activityIndicator: FrameLayout?=null
    var interntConnected: LinearLayout?=null
    var isInternet:Boolean = true


    fun init(view: View){
        tv_basic = view.findViewById(R.id.tv_basic)
        loadingIndicatior= view.findViewById(R.id.avIndicator)
        activityIndicator= view.findViewById(R.id.activityIndicator)
        interntConnected = view.findViewById(R.id.ll_interntConnected)

        AgentReachability.get(context!!).startListeningForNetworkChanges(context!!)
        if(AgentReachability.get(context!!).networkConnected()!!){
            hideNoInternetWarning()
        }else{
            showNoInternetWarning()
        }

        LocalBroadcastManager.getInstance(context!!).registerReceiver(mMessageReceiver,
            IntentFilter(AgentReachability.GPR_REACHABILITY_CHANGED_EVENT)
        )
    }

    fun showIndicator(text:String){
        activityIndicator?.visibility = View.VISIBLE

        tv_basic?.setText(text)

    }

    fun hideIndicator(){
        activityIndicator?.visibility = View.GONE

    }

    fun showNoInternetWarning() {
        interntConnected?.visibility = View.VISIBLE
    }

    fun hideNoInternetWarning() {
        interntConnected?.visibility = View.GONE
    }

    @SuppressLint("ServiceCast")
    fun isNetworkConnected():Boolean{
        val connectivityManager=activity?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo=connectivityManager.activeNetworkInfo
        return  networkInfo!=null && networkInfo.isConnected
    }



    private val mMessageReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            // Get extra data included in the Intent
            val networkConnected = intent.getBooleanExtra(AgentReachability.GPR_REACHABILITY_INTERNET_CONNECTED,false)
            if(networkConnected){
                hideNoInternetWarning()
                isInternet = true
            }else{
                isInternet = false
                showNoInternetWarning()
            }
        }
    }
}