package com.theagent.runtimePermission

import android.content.Context
import androidx.appcompat.app.AlertDialog
import com.fondesa.kpermissions.extension.flatString
import com.fondesa.kpermissions.request.PermissionRequest
import com.fondesa.kpermissions.request.runtime.nonce.PermissionNonce
import com.theagent.R

class DialogRationaleListener(private val context: Context?) : PermissionRequest.RationaleListener {
    override fun onPermissionsShouldShowRationale(
        permissions: Array<out String>,
        nonce: PermissionNonce
    ) {
        val msg = String.format(
            context!!.getString(R.string.rationale_permissions),
            permissions.flatString()
        )

        AlertDialog.Builder(context)
            .setTitle(R.string.permissions_required)
            .setMessage(msg)
            .setPositiveButton(R.string.request_again) { _, _ ->
                // Send the request again.
                nonce.use()
            }
            .setNegativeButton(android.R.string.cancel, null)
            .show()
    }
}